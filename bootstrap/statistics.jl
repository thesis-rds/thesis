using Statistics, HypothesisTests, StatsPlots, Distributions
results = [load_results(groups, "bc_complete_alpha_1"), load_results(groups, "bc_complete_alpha_2"), load_results(groups, "bc_complete_alpha_0")]
results_again = load_results(groups_again, "bc_complete_again_20_alpha_0")

d_a1 = Dict([group.name => group for group in results[1]])
d_a2 = Dict([group.name => group for group in results[2]])
d_af = Dict([group.name => group for group in results[3]])

r1_f = filter(x -> x.class == "F", results[1])
rf_f = filter(x -> x.class == "F", results[3])
r1_d = filter(x -> x.class == "D", results[1])
rf_d = filter(x -> x.class == "D", results[3])

i1t2_f = map(x -> d_a1[x.name].q - d_a2[x.name].q, r1_f)
i1tf_f = map(x -> d_a1[x.name].q - d_af[x.name].q, rf_f)
i1t2n_f = map(x -> (d_a1[x.name].q - d_a2[x.name].q) / d_a1[x.name].q, r1_f)
i1tf_d = map(x -> d_a1[x.name].q - d_af[x.name].q, rf_d)
i1t2n_d = map(x -> (d_a1[x.name].q - d_a2[x.name].q) / d_a1[x.name].q, r1_d)



average_improve_f = mean(i1tf_f)
std_ai_f = std(i1tf_f)
average_improve_d = mean(i1tf_d)
std_ai_d = std(i1tf_d)
p = pvalue(UnequalVarianceTTest(i1tf_d, i1tf_f))

function alpha_distr(αVec::Vector{Float64})
    αMin = minimum(αVec) - 1
    αMax = maximum(αVec) + 1
    X = αMin:0.01:αMax
    αDistr = reduce(+, [ pdf.(Normal(α, 0.1), X) ./ 4 for α in αVec])
    (X, αDistr)
end
