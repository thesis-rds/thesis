##
## Adapter to exchange information between Julia & Angular
## & some plotting
##

function compileFilteredMeta()
    loadAllFiltered()
    meta = get_all_metadata()
    for f in Thesis.DataFiltered
        cmeta = compile_meta(f)
        if !f.name in meta
            meta[f.name] = cmeta
        else
            for key in cmeta
                meta[f.name][key] = cmeta[key]
            end
        end
    end
    store_all_metadata(meta)
end
