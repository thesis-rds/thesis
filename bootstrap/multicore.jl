# * Fix the EPSP
# * Fix recovery peak
# * Cluster the results (classification)
# * Math: no more than double, facilitating
# * 1 frequency band
# * Is model invariant of frequency?
# * Fit independently of frequency

using Distributed
@everywhere include("Thesis.jl")
@everywhere include("src/results_chi.jl")
using Main.Thesis, DotEnv
DotEnv.config()

if length(ARGS) > 0
    α = ARGS[1]
else
    α = ENV["RUN_ALPHA"]
end
run_name = ENV["RUN_NAME"] * "_alpha_" * α
α = parse(Int64, α)

loadAllMinimized()
traces = Main.Thesis.DataMinimized
d_traces = Main.Thesis.DictMinimized



# # Create
# create_results(run_name, traces, α)
#
# # Improve
# improve_results(run_name, traces, α)

# Grouped
groups = [
    TraceGroup("c53",[
        d_traces["c53h20"],
        d_traces["c53h40"],
        d_traces["c53h70"],
    ],
    [0. 4.; 0. 300.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("c63",[
        d_traces["c63h20"],
        d_traces["c63h40"],
        d_traces["c63h70"],
    ],
    [0. 4.; 0. 400.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("cfacil_65",[
        d_traces["cfacil_65h40"]
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 3.],
    "F"
    ),
    TraceGroup("conn12",[
        d_traces["conn12_20hz"],
        d_traces["conn12_40hz"],
        d_traces["conn12_70hz"],
    ],
    [0. 4.; 0. 400.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("conn52",[
        d_traces["conn52_20hz"],
        d_traces["conn52_40hz"],
        d_traces["conn52_70hz"],
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("cp2ifac43",[
        d_traces["cp2ifac43h40"],
        d_traces["cp2ifac43h70"],
    ],
    [0. 4.; 0. 250.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("cp2ifac65",[
        d_traces["cp2ifac65h20"],
        d_traces["cp2ifac65h40"],
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("facil_25",[
        d_traces["facil_25h20"],
        d_traces["facil_25_h40"],
    ],
    [0. 4.; 0. 400.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("i2pdep46",[
        d_traces["i2pdep46h20"]
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "D"
    ),
    TraceGroup("mean12",[
        d_traces["mean12h20"]
    ],
    [0. 4.; 0. 300.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("meanp2i14",[
        d_traces["meanp2i14h40"]
    ],
    [0. 4.; 0. 400.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("p2idep24",[
        d_traces["p2idep24h20"]
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "D"
    ),
    TraceGroup("p2idep25",[
        d_traces["p2idep25h30"]
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "D"
    ),
    TraceGroup("p2idep35",[
        d_traces["p2idep35h20"],
        d_traces["p2idep35h40"],
    ],
    [0. 4.; 0. 300.; 0. 1.; 0. 2.; 0. 2.],
    "D"
    ),
    TraceGroup("p2idep54",[
        d_traces["p2idep54h20"]
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "D"
    ),
    TraceGroup("p2idep64",[
        d_traces["p2idep64h20"]
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "D"
    ),
    TraceGroup("p2idep65",[
        d_traces["p2idep65h20"]
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "D"
    ),
    TraceGroup("p2ifac13",[
        d_traces["p2ifac13h20"],
        d_traces["p2ifac13h40"],
        d_traces["p2ifac13h70"],
    ],
    [0. 4.; 0. 400.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("p2ifac14_1",[
        d_traces["p2ifac14h20"],
        d_traces["p2ifac14h40"],
        d_traces["p2ifac14h70"],
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("p2ifac14_2",[
        d_traces["p2ifac14_h40"],
        d_traces["p2ifac14_h70"],
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("p2ifac15",[
        d_traces["p2ifac15h40"]
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("p2ifac16",[
        d_traces["p2ifac16h40"],
        d_traces["p2ifac16h70"],
    ],
    [0. 4.; 0. 400.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("p2ifac25",[
        d_traces["p2ifac25h20"],
        d_traces["p2ifac25h40"],
    ],
    [0. 4.; 0. 300.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("p2ifac36",[
        d_traces["p2ifac36h20"],
        d_traces["p2ifac36h40"],
        d_traces["p2ifac36h70"],
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("p2ifac53",[
        d_traces["p2ifac53h40"],
        d_traces["p2ifac53h70"],
    ],
    [0. 4.; 0. 400.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("p2ifac54",[
        d_traces["p2ifac54h40"],
        d_traces["p2ifac54h70"],
    ],
    [0. 4.; 0. 300.; 0. 1.; 0. 2.; 0. 3.],
    "F"
    ),
    TraceGroup("p2ifac62",[
        d_traces["p2ifac62h20"]
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("p2ifac63_1",[
        d_traces["p2ifac63h20"],
        d_traces["p2ifac63h40"],
        d_traces["p2ifac63h70"],
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 4.; 0. 2.],
    "F"
    ),
    TraceGroup("p2ifac63_2",[
        d_traces["p2ifac63_h20"],
        d_traces["p2ifac63_h70"],
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 4.; 0. 2.],
    "F"
    ),
    TraceGroup("p2ifac64",[
        d_traces["p2ifac64h40"],
        d_traces["p2ifac64h70"],
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("p2ifacil15",[
        d_traces["p2ifacil15h70"]
    ],
    [0. 4.; 0. 400.; 0. 1.; 0. 2.; 0. 2.],
    "F"
    ),
    TraceGroup("p2p16",[
        d_traces["p2p16h20"]
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "D"
    ),
    TraceGroup("p2p23",[
        d_traces["p2p23h30"]
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "D"
    ),
    TraceGroup("p2p26",[
        d_traces["p2p26h30"]
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "D"
    ),
    TraceGroup("p2p61",[
        d_traces["p2p61h20"]
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "D"
    ),
    TraceGroup("p2pdep52",[
        d_traces["p2pdep52h20"]
    ],
    [0. 4.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
    "D"
    )
]

groups_again = [
    TraceGroup("p2ifac14_1",
        [
            d_traces["p2ifac14h20"],
            d_traces["p2ifac14h40"],
            d_traces["p2ifac14h70"],
        ],
        [0. 10.; 0. 200.; 0. 1.; 0. 2.; 0. 2.],
        "F"
    ),
    TraceGroup("p2ifac63_2",
        [
            d_traces["p2ifac63_h20"],
            d_traces["p2ifac63_h70"],
        ],
        [10. 20.; 0. 200.; 0. 1.; 0. 4.; 0. 2.],
        "F"
    ),
    TraceGroup("p2ifac13",
        [
            d_traces["p2ifac13h20"],
            d_traces["p2ifac13h40"],
            d_traces["p2ifac13h70"],
        ],
        [10. 20.; 0. 400.; 0. 1.; 0. 2.; 0. 2.],
        "F"
    ),
]

loadAllFinal()

all_traces = vcat(map(
    group -> map(trace -> Main.Thesis.DictFinal[trace.name], group.traces),
    groups
)...)

# create_grouped_results(run_name, groups_again, α)
