## DO NOT USE, FOR REFERENCE/BACKWARD-COMPAT ONLY
## Use multicore.jl instead.

include("../Thesis.jl")
include("../src/results.jl")
using Main.Thesis

run_name = "grouping_test_a2_1"
α = 2

loadAllMinimized()
traces = Main.Thesis.DataMinimized
d_traces = Main.Thesis.DictMinimized



# # Create
# create_results(run_name, traces, α)
#
# # Improve
# improve_results(run_name, traces, α)

# Grouped
groups = [
    TraceGroup("c53",[
        d_traces["c53h20"],
        d_traces["c53h40"],
        d_traces["c53h70"],
    ],
    [0. 200.; 0. 1.; 0. 2.; 0. 2.]),
    TraceGroup("c63",[
        d_traces["c63h20"],
        d_traces["c63h40"],
        d_traces["c63h70"],
    ],
    [0. 200.; 0. 1.; 0. 2.; 0. 2.]),
    TraceGroup("conn52",[
        d_traces["conn52_20hz"],
        d_traces["conn52_40hz"],
        d_traces["conn52_70hz"],
    ],
    [0. 200.; 0. 1.; 0. 2.; 0. 2.]),
    TraceGroup("cp2ifac43",[
        d_traces["cp2ifac43h40"],
        d_traces["cp2ifac43h70"],
    ],
    [0. 200.; 0. 1.; 0. 2.; 0. 2.]),
    TraceGroup("cp2ifac65",[
        d_traces["cp2ifac65h20"],
        d_traces["cp2ifac65h40"],
    ],
    [0. 200.; 0. 1.; 0. 2.; 0. 2.]),
    TraceGroup("facil_25",[
        d_traces["facil_25h20"],
        d_traces["facil_25_h40"],
    ],
    [0. 200.; 0. 1.; 0. 2.; 0. 2.]),
    TraceGroup("p2idep35",[
        d_traces["p2idep35h20"],
        d_traces["p2idep35h40"],
    ],
    [0. 300.; 0. 1.; 0. 2.; 0. 2.]),
    TraceGroup("p2ifac13",[
        d_traces["p2ifac13h20"],
        d_traces["p2ifac13h40"],
        d_traces["p2ifac13h70"],
    ],
    [0. 200.; 0. 1.; 0. 2.; 0. 2.]),
    TraceGroup("p2ifac14",[
        d_traces["p2ifac14_h40"],
        d_traces["p2ifac14_h70"],
        d_traces["p2ifac14h20"],
        d_traces["p2ifac14h40"],
        d_traces["p2ifac14h70"],
    ],
    [0. 200.; 0. 1.; 0. 2.; 0. 2.]),
    TraceGroup("p2ifac16",[
        d_traces["p2ifac16h40"],
        d_traces["p2ifac16h70"],
    ],
    [0. 200.; 0. 1.; 0. 2.; 0. 2.]),
    TraceGroup("p2ifac25",[
        d_traces["p2ifac25h20"],
        d_traces["p2ifac25h40"],
    ],
    [0. 200.; 0. 1.; 0. 2.; 0. 2.]),
    TraceGroup("p2ifac36",[
        d_traces["p2ifac36h20"],
        d_traces["p2ifac36h40"],
        d_traces["p2ifac36h70"],
    ],
    [0. 200.; 0. 1.; 0. 2.; 0. 2.]),
    TraceGroup("p2ifac53",[
        d_traces["p2ifac53h40"],
        d_traces["p2ifac53h70"],
    ],
    [0. 200.; 0. 1.; 0. 2.; 0. 2.]),
    TraceGroup("p2ifac54",[
        d_traces["p2ifac54h40"],
        d_traces["p2ifac54h70"],
    ],
    [0. 200.; 0. 1.; 0. 2.; 0. 2.]),
    TraceGroup("p2ifac63",[
        d_traces["p2ifac63_h20"],
        d_traces["p2ifac63h20"],
        d_traces["p2ifac63h40"],
        d_traces["p2ifac63h70"],
    ],
    [0. 200.; 0. 1.; 0. 2.; 0. 2.]),
    TraceGroup("p2ifac64",[
        d_traces["p2ifac64h40"],
        d_traces["p2ifac64h70"],
    ],
    [0. 200.; 0. 1.; 0. 2.; 0. 2.]),
    TraceGroup("conn12",[
        d_traces["conn12_20hz"],
        d_traces["conn12_40hz"],
        d_traces["conn12_70hz"],
    ],
    [0. 200.; 0. 1.; 0. 2.; 0. 2.])
]

create_grouped_results(run_name, groups, α)
