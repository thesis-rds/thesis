using Plots, Plots.PlotMeasures
plotly()

sd_a1 = sort(d_a1, by= x->d_a1[x].q)
sd_a2 = sort(d_a2, by= x->d_a1[x].q)
sd_af = sort(d_af, by= x->d_a1[x].q)

scatter(
    [
        -1,
        1:length(sd_a1)...
    ],
    [
        0,
        map(x->x.q,values(sd_a1))
    ],
    color = [
        RGBA(0.5, 0.5, 0.5, 0.1),
        [
            x.class == "F" ? :orange : :blue
            for (key, x) in sd_a1
        ]...
    ],
    label = "Alpha fixed at 1",
    opacity = 0.4,
    hover = [
        "?",
        [x.name for (k, x) in sd_a1]...
    ]
)

p1 = scatter!(
    [
        -1,
        1:length(sd_af)...
    ],
    [
        0,
        map(x->x.q,values(sd_af))...
    ],
    color = [
        RGBA(0.5, 0.5, 0.5, 1.),
        [
            x.class == "F" ? :orange : :blue
            for (key, x) in sd_af
        ]...
    ],
    label = "Alpha as free parameter",
    hover = [
        "?",
        [x.name for (k, x) in sd_a1]...
    ]
)

scatter!([-1], [0] , color=:orange, label="Facilitating")
scatter!([-1], [0], color=:blue, label="Depressing")

figure1 = plot(
    p1,
    title="Goodness of fit",
    xlabel="Samples\nsorted by Q",
    ylabel="Q value of χ²",
    size=(1000,500),
    xlims=(0,36),
    # xticks = false,
    legendfontsize = 16,
    titlefontsize = 28,
    guidefontsize = 16,
    xformatter = v -> "",
    left_margin = 30px,
    top_margin = 30px
)

xaxis!(font(16))


figure2 = bar(
    [average_improve_f, average_improve_d],
    title="Comparison of improvement in goodness of fit",
    ylabel="Average decrease in Q of χ²",
    xlabel="p < 0.009",
    color=[:orange,:blue],
    ylims=(0,0.1),
    grid=false,
    bar_width = 0.4,
    xlims=(0,3),
    tick_direction = :out,
    xformatter = v -> v == 1. ? "Facilitating" : v == 2. ? "Depressing" : "",
    legend=false,
    titlefontsize=16,
    top_margin=15px,
    left_margin=15px,
    xtickfontsize=12,
    yguidefontsize=14,
    xguidefontsize=8,
    size=(550,550)
)

annotate!([
    (1., average_improve_f + 0.005, text(string(round(average_improve_f, sigdigits=3)), 10)),
    (2., average_improve_d + 0.005, text(string(round(average_improve_d, sigdigits=3)), 10))
])

add_to_legend = true
function plot_reconstruction_panel(trace, original, predicted, xlabel="", ylabel = "", ymax = 1, title = "")
    global add_to_legend
    panel = plot(collect(1:length(trace.final)) * trace.Δt, trace.final .- trace.baseline,label=add_to_legend ? "Deconvoluted signal" : "")
    scatter!(trace.points.X * trace.Δt, original,label=add_to_legend ? "Peaks" : "")
    scatter!(trace.points.X * trace.Δt, predicted,label=add_to_legend ? "Predicted peaks" : "")
    if !add_to_legend
        plot!(label=false)
    end
    if ylabel != ""
        plot!(ylabel=ylabel)
    end
    if xlabel != ""
        plot!(xlabel=xlabel)
    end
    if title != ""
        plot!(title=title)
    end
    plot!(ylims=(-1, ymax + 1))
    add_to_legend = false
    panel
end

group_name = "p2ifac63_2"
# group_name = "cfacil_65"
# group_name = "p2ifac63_2"
# group_name = "p2ifac53"
d_again = Dict(map(x->x.name => x,results_again))
d_af_c = Dict(map(x->(x.name => (if x.name in keys(d_again) d_again[x.name] else d_af[x.name] end)),values(d_af)))
group = filter(x -> x.name==group_name, groups)[1]
original_peaks = map(x -> x.points.Y .- x.baseline,group.traces)
new_peaks_a1 = map(x->TM_ext(x.points.X .* x.Δt, d_a1[group.name].parameters...), group.traces)
new_peaks_af = map(x->TM_ext(x.points.X .* x.Δt, d_af_c[group.name].parameters...), group.traces)
max_peak = maximum(vcat(original_peaks..., new_peaks_a1..., new_peaks_af...))
hertz = ["20Hz", "40Hz", "70Hz"]

top_panels = [
    plot_reconstruction_panel(
        group.traces[i],
        original_peaks[i],
        new_peaks_a1[i],
        "",
        i == 1 ? "Membrane excitation (mV, α = " * string(d_a1[group.name].parameters[1]) * ")" : "",
        max_peak,
        hertz[i]
    ) for i = 1:length(group.traces)
]

bottom_panels = [
    plot_reconstruction_panel(
        group.traces[i],
        original_peaks[i],
        new_peaks_af[i],
        i == 2 ? "Time (s)" : "",
        i == 1 ? "Membrane excitation (mV, α = " * string(round(d_af_c[group.name].parameters[1], sigdigits=3)) * ")" : "", max_peak
    ) for i = 1:length(group.traces)
]

figure3 = plot(
    plot(top_panels..., layout=(1,length(group.traces))),
    plot(bottom_panels..., layout=(1,length(group.traces))),
    left_margin = 40px,
    layout=(2,1),
    size=(1400,700),
    markersize=3,
    legendfontsize=14
)

figure4 = plot(
    histogram(map(x->x.parameters[1],values(d_af)), ylabel="All samples", title="Distribution of α as free parameter", bins=0:0.2:4, color=:gray),
    histogram(map(x->x.parameters[1],values(rf_f)), ylabel="Facilitating", color=:orange, bins=0:0.2:4),
    histogram(map(x->x.parameters[1],values(rf_d)), ylabel="Depressing", bins=0:0.2:4, color=:blue, xlabel="Alpha"),
    layout = (3,1),
    heights = [0.33, 0.33, 0.33],
    grid = false,
    ylims = (0.05, 4.01),
    tick_direction = :out,
    legend = false,
    top_margin=10px,
    left_margin = 40px,
)

λ = 10

times_per = collect(LinRange(0.,1.,λ))
times_uni = sort(rand(λ))
times_poi = poisson_until(λ, 1.)

param_d = [1., 100., 0.7, .02, 1.7]
param_m = [1., 100., .25, .2, .2]
param_f = [1., 100., .1, 1.7, .02]

example_points = []
push!(example_points, [times_per, TM_ext(times_per, param_d...)])
push!(example_points, [times_per, TM_ext(times_per, param_m...)])
push!(example_points, [times_per, TM_ext(times_per, param_f...)])
push!(example_points, [times_uni, TM_ext(times_uni, param_d...)])
push!(example_points, [times_uni, TM_ext(times_uni, param_m...)])
push!(example_points, [times_uni, TM_ext(times_uni, param_f...)])
push!(example_points, [times_poi, TM_ext(times_poi, param_d...)])
push!(example_points, [times_poi, TM_ext(times_poi, param_m...)])
push!(example_points, [times_poi, TM_ext(times_poi, param_f...)])

function example_points_plot(timestamps, responses)
    scatter(timestamps, responses)
    # plot!([10, 10], [1000, 1], arrow=100.)
end

figure_example = plot(map(x->example_points_plot(x[1],x[2]),example_points)...,
    layout=(3,3),
    xlims=(0.,1.),
    ylims=(0,100),
    legend=false,
    grid=false,
)

filter_trace = Main.Thesis.DataFinal[33]
dt = 0.00025
timestamps = range(0., step=0.00025, length=length(filter_trace.filtered) - 1)
figure_filter_panel1 = plot(timestamps, filter_trace.raw, opacity= 0.2, linewidth=4, color=:red,label="Unfiltered")
plot!(timestamps, filter_trace.filtered,
    opacity= 1,
    linewidth=1,
    color=:red,
    ylabel="Membrane potential (mV)",
    xlabel="Time (s)",
    label="Filtered"
);
figure_filter_panel2 = plot(timestamps, filter_trace.raw,
    opacity= 0.2,
    linewidth=4,
    color=:red,
    ylims=(-70.3, -70.5),
    xlims=(0.18,0.23),
    label="",
)
plot!(timestamps, filter_trace.filtered,
    opacity= 1,
    linewidth=1,
    color=:red,
    ylabel="Membrane potential (mV)",
    xlabel="Time (s)",
    label="",
);

figure_filter = plot(figure_filter_panel1,figure_filter_panel2,layout=(1,2),size=(800,300),legendfontsize=11,guidefontsize=12)

plot(timestamps, deconvolute(filter_trace.raw, filter_trace.τ, 0.00025),
    opacity=0.5,
    label="Unfiltered deconvolution"
)
figure_filter_use = plot!(timestamps, deconvolute(filter_trace.filtered, filter_trace.τ, 0.00025),
    label="Prefiltered deconvolution",
    xlabel="Time (s)",
    ylabel="Potential (mV)",
    left_margin=20px,
    size=(800,400)
)


raw_minus_filtered_amp = vcat(map(
    trace -> abs.(
        trace.raw[map(x->Int64(x),trace.peaks.X)]
        .- trace.filtered[map(x->Int64(x),trace.peaks.X)]
    ),
    copy(all_traces)
)...)

import Base.-
-(x::Pair{Int64,Int64}, y::Int64) = abs(x[1] - y) => x[2]
-(x::Pair{Int64,Float64}, y::Int64) = abs(x[1] - y) => x[2]

time_shift = vcat(map(
    trace -> (
        candidates = map(peak -> peak => peak, peaks(trace.raw, 10));
        abs.(trace.peaks.X .- map(
            response_peak -> minimum(candidates .- response_peak)[2],
            convert(Vector{Int64}, trace.peaks.X)
        ))
    ),
    all_traces
)...) .* 0.00025

unfiltered_deconv_pc = vcat(map(
    trace -> (
        deconv = deconvolute(trace.raw,trace.τ,trace.Δt);
        candidates = map(peak -> peak => peak, peaks(deconv, 40));
        unfiltered_peaks = map(
            response_peak -> minimum(candidates .- response_peak)[2],
            convert(Vector{Int64}, trace.peaks.X)
        );
        hcat(unfiltered_peaks .- trace.peaks.X, deconv[unfiltered_peaks] .- trace.peaks.Y)
    ),
    all_traces
)...)

mean_amp_shift = mean(raw_minus_filtered_amp)
std_amp_shift = std(raw_minus_filtered_amp)
mean_time_shift = mean(time_shift)
std_time_shift = std(time_shift)

amplitude_shift_panel = boxplot(raw_minus_filtered_amp,
    xlims=(0.,2.),
    ylims=(-0.1,1.),
    size=(400,400),
    label="",
    xlabel="Filtered",
)
amp_shift_unf = boxplot(
    ylabel="Amplitude shift (mV)",
    xlims=(0.,2.),
    unfiltered_deconv_pc[:,2],
    label="",
    xlabel="Unfiltered",
)

time_shift_panel = boxplot(time_shift,
    xlims=(0.,2.),
    ylabel="Time shift (ms)",
    ylims=(-0.025,0.25),
    size=(400,400),
    label="Response peak",
    xlabel="Filtered",
)

filter_layout = @layout [grid(1,3)
                         b{0.5h}]

# display(plot(rand(10,4), layout=filter_layout))

figure_filter_complete = plot(
    amp_shift_unf,
    amplitude_shift_panel,
    time_shift_panel,
    figure_filter_use,
    xticks=false,
    left_margin=15px,
    layout=filter_layout,
    legendfontsize=12,
    guidefontsize=12,
    size=(800,600)
)

sample_trace_1 = all_traces[16]
peak_selection_figure = plot(
    range(0., step=sample_trace_1.Δt, length=length(sample_trace_1.filtered)),
    sample_trace_1.filtered,
    label="Trace recording",
    ylabel="Membrane potential (mV)",
    xlabel="Time (s)"
)
scatter!(
    peaks_pc(sample_trace_1.filtered, 20).X .* sample_trace_1.Δt,
    peaks_pc(sample_trace_1.filtered, 20).Y,
    opacity=0.3,
    markersize=3,
    label="Detected peaks"
)
scatter!(
    [sample_trace_1.peaks.X..., peaks_pc(sample_trace_1.filtered, 20).X[end-2]] .* sample_trace_1.Δt,
    [sample_trace_1.peaks.Y..., peaks_pc(sample_trace_1.filtered, 20).Y[end-2]],
    markersize=3,
    label="Manually selected peaks"
)

epsp_poi = uniform_epsp(times_poi, 1., 0.00025)
corr_deconv = deconvolute(epsp_poi, 0.04, 0.00025)
under_deconv = deconvolute(epsp_poi, 0.01, 0.00025)
over_deconv = deconvolute(epsp_poi, 0.16, 0.00025)
deconv_peaks = peaks_pc(corr_deconv, 20)
deconv_peaks = PointCollection(deconv_peaks.X, deconv_peaks.Y ./ deconv_peaks.Y[1])
under_peaks = peaks_pc(under_deconv, 20)
under_peaks = PointCollection(under_peaks.X, under_peaks.Y ./ under_peaks.Y[1])
over_peaks = peaks_pc(over_deconv, 20)
over_peaks = PointCollection(over_peaks.X[1:(end-1)], over_peaks.Y[1:(end-1)] ./ over_peaks.Y[1])

estimation_tau_panel = plot(
    range(0.,step=0.00025, length=length(corr_deconv)),
    corr_deconv ./ peaks_pc(corr_deconv, 20).Y[1],
    label="Optimally deconvoluted trace",
)
scatter!(
    deconv_peaks.X .* 0.00025,
    deconv_peaks.Y,
    label="Correct τ",
)
scatter!(
    under_peaks.X .* 0.00025,
    under_peaks.Y,
    label="Underestimated τ",
)
scatter!(
    over_peaks.X .* 0.00025,
    over_peaks.Y,
    label="Overestimated τ",
)

original_trace_panel = plot(
    range(0.,step=0.00025, length=length(epsp_poi)),
    epsp_poi,
    linecolor=:black,
    label="Poisson distr. input response",
)

under_panel = plot(
    range(0.,step=0.00025, length=length(under_deconv)),
    under_deconv ./ peaks_pc(under_deconv, 20).Y[1],
    label="",
    color=:green,
)

over_panel = plot(
    range(0.,step=0.00025, length=length(over_deconv)),
    over_deconv ./ peaks_pc(over_deconv, 20).Y[1],
    label="",
    xlabel="Time (s)",
    color=:purple,
)

estimation_tau_figure = plot(
    original_trace_panel,
    estimation_tau_panel,
    under_panel,
    over_panel,
    size=(1000,600),
    layout=(4,1),
    legendfontsize=12,
    xguidefontsize=16,
    yguidefontsize=10,
)

display(estimation_tau_figure)
