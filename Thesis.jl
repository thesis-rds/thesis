module Thesis

    using Distributions, Plots
    plotly()

    include("src/peaks.jl")
    export peaks, peaks_pc, baseline, PointCollection
    include("src/trace.jl")
    export AbstractTrace, AbstractRawTrace, AbstractFilteredTrace, AbstractAnnotatedTrace, AbstractDeconvolutedTrace, AbstractFinalTrace
    export RawTrace, FilteredTrace, AnnotatedTrace, DeconvolutedTrace, FinalTrace, MinimizedTrace
    export getTraceTypeString, getTraceType
    include("src/io.jl")
    export saveTrace, loadRawTrace, loadFilteredTrace, loadAnnotatedTrace, loadDeconvolutedTrace, loadFinalTrace, loadMinimizedTrace, toHTML
    export loadAllRaw, loadAllFiltered, loadAllAnnotated, loadAllDeconvoluted, loadAllFinal, loadAllMinimized, loadAll
    include("src/plot.jl")
    export plotTrace
    include("src/convolution.jl")
    export deconvolute, reconvolute
    include("src/poisson.jl")
    export poisson_until
    include("src/generate.jl")
    export uniform_epsp, peak_epsp
    include("src/lms.jl")
    export lms, lms_sweep
    include("src/sgf.jl")
    export SavitzkyGolayFilter
    include("src/tm.jl")
    export TM, TM_ext
    include("src/anneal.jl")
    export anneal, tm_model_factory, tm_cost_factory, tm_model_multi_factory, tm_model_group_factory

    include("src/stage/advance.jl")
    include("src/stage/build.jl")
    include("src/stage/compile.jl")
    include("src/stage/meta.jl")
    export advance, build, compile, from_meta, get_all_metadata
    export drop, restore

    Data = AbstractTrace[]

    DataRaw = AbstractRawTrace[]
    DataFiltered = AbstractFilteredTrace[]
    DataAnnotated = AbstractAnnotatedTrace[]
    DataDeconvoluted = AbstractDeconvolutedTrace[]
    DataFinal = FinalTrace[]
    DictFinal = Dict{String, FinalTrace}()
    DataMinimized = MinimizedTrace[]
    DictMinimized = Dict{String, MinimizedTrace}()

    function resetMinimized()
        DataMinimized = MinimizedTrace[]
    end
    export resetMinimized

end
