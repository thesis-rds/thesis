A  = 1			# Amplitude
U  = 0.25		# Use-fraction per event
F  = 0.2		# Facilitation of use-fraction per event
D  = 0.2		# Depression of ready-fraction per event

f  = 40			# Event frequency (Hz, 1/s)
T  = 10			# Duration (s)
Δt = 0.001		# Time step

# Quick-use table		U		F		D
# Strong depression:    0.7		0.02	1.70
# Depression:		    0.5		0.05	0.50
# F-D:			        0.25	0.20	0.20
# Facilitation:		    0.15	0.50	0.05
# Strong facilitation:	0.1		1.70	0.02

timestamps = poisson_until(f, T)
sim_input  = uniform_epsp(timestamps, T, Δt)
deconv = deconvolute(sim_input, 0.04, Δt)
plot(sim_input)
display(plot!(deconv))

f = FilteredTrace(Thesis.Data[1], 30, 2)
saveTrace(f)
b, bx = baseline(f.raw, -62.6)
br = PointCollection([1, bx],[minimum(f.raw[1:bx]),f.raw[bx]])
a = AnnotatedTrace(f, b, br, peaks_pc(f.filtered, 25)[6,7,9,10,11,12,12,14,15])

plot(); plotTrace(a)
