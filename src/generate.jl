#StdEPSP config
GSE_a1 = 0.636
GSE_a2 = -2.01
GSE_a3 = 1.34
GSE_tau = [0.001, 0.003, 0.040] #s

"""
	uniform_epsp(timestamps, amplitudes, tmax, dt)

Create an EPSP train with uniform peak amplitudes
"""
function uniform_epsp(timestamps, tmax, dt)
	N = Int(floor(tmax/dt))
	tau1 = GSE_tau[1] / dt
	tau2 = GSE_tau[2] / dt
	tau3 = GSE_tau[3] / dt
	V = zeros(N)
	for j = 1:length(timestamps) #Loop over timestamps and overlay (sum) them on V
		t0 = round(Int,timestamps[j] / dt)
		for i = t0:N
			t = -i + t0 - 0.0004462362 / dt #Offset for the initial dip
			V[i] = V[i] + GSE_a1*exp(t/tau1) + GSE_a2*exp(t/tau2) + GSE_a3*exp(t/tau3)
		end
	end
	return V
end

"""
	peak_epsp(timestamps, amplitudes, tmax, dt)

Create an EPSP train with different peak amplitudes.
"""
function peak_epsp(timestamps, amplitudes, tmax, dt)
	# Calculate seperate EPSP's from timestamps, multiply each EPSP by their peak amplitude and sum together.
	return sum([amplitudes[i] * uniform_epsp(timestamps[i], tmax, dt) for i = 1:length(timestamps)])
end
