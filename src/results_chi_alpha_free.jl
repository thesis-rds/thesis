using Main.Thesis, Plots, JSON, SharedArrays, Distributed
plotly()

function chisq(values, model_values)
    sum((values .- model_values) .^ 2)
end

function multistart(model, p_min, p_max, iter = 100)
    best_E = typemax(Float64)
    errors = SharedArray{Float64}(iter)
    parameters = [SharedArray{Float64}(length(p_min)) for _ = 1:iter]
    @sync @distributed for i in 1:iter
        p = rand() .* (p_max .- p_min) .+ p_min
        E, p = anneal(model, p, p ./ 5., p_min, p_max, 100000)
        errors[i] = E
        for j = 1:length(p_min)
            parameters[i][j] = p[j]
        end
    end
    best_p = parameters[argmin(errors)]
    best_p
end

struct TraceGroup
    name::String
    traces::Vector{AbstractTrace}
    param_range::Matrix{Float64}
end

"""Returns chi square of TM_ext for the given parameters and TraceGroup"""
function get_group_error(group::TraceGroup, parameters)
    sum_error = 0.
    for trace_m in group.traces
        expected = TM_ext(trace_m.points.X .* trace_m.Δt, parameters...)
        observed = trace_m.points.Y .- trace_m.baseline
        sum_error = sum_error + sum(((observed .- expected) .^ 2) ./ expected)
    end
    sum_error
end

function chisq_handler(group, params)
    e = get_group_error(group, params)
end

function create_grouped_results(name, groups)
    meta = get_all_metadata()
    if isfile("../thesis_data/results/" * name * ".json")
        results = load_results(name)
    else
        results = Dict()
    end
    errors = Dict()
    for kv in results
        errors[kv[1]] = get_group_error(group, kv[2])
    end
    try
        mkdir("../thesis_data/result_plots/" * name)
    catch
    end
    for group in groups
        group_count = 0
        multi_model = []
        for trace_m in group.traces
            if meta[trace_m.name]["curation"] == "minimized"
                group_count = group_count + 1
            end
        end
        if group_count > 0
            old_err = haskey(errors, group.name) ? errors[group.name] : typemax(Float64)
            old_params = Nothing
            try
                mkdir("../thesis_data/result_plots/" * name * "/" * group.name)
            catch
            end
            for group_repeats = 1:7
                model = tm_model_group_af_factory(group, chisq_handler)
                print("annealing ", group.name, "\n")
                res = multistart(model, group.param_range[:, 1], group.param_range[:, 2], 200)
                sum_error = get_group_error(group, res)
                if sum_error < old_err
                    print("** new err:", sum_error, "\nold err:", old_err, "\n")
                    for trace_m in group.traces
                        model_peaks = TM_ext(trace_m.points.X .* trace_m.Δt, res...)
                        plot(); plotTrace(trace_m); scatter!(trace_m.points.X, model_peaks .+ trace_m.baseline, markersize=3)
                        if old_params != Nothing
                            old_peaks = TM_ext(trace_m.points.X .* trace_m.Δt, res...)
                            scatter!(trace_m.points.X, old_peaks .+ trace_m.baseline, markersize=3, opacity=0.4)
                        end
                        savefig("../thesis_data/result_plots/" * name * "/" * group.name * "/" * trace_m.name * ".html")
                    end
                    old_params = res
                    old_err = sum_error
                    results[group.name] = res
                    errors[group.name] = sum_error
                    save_raw_results(name, results)
                end
            end
        end
    end
    plot(); scatter(1:length(errors), collect(values(errors)), hover = collect(keys(errors)))
    savefig("../thesis_data/error_plots/" * name * "_norm.html")
    save_raw_results(name, results)
    results, errors
end

function plot_errors(name, errors = Nothing)
    if errors == Nothing
        f = open("../thesis_data/results/" * name * ".json", read = true)
        errors = JSON.parse(read(f, String))
        close(f)
    end
    plt = scatter(1:length(errors), collect(values(errors)), hover = collect(keys(errors)))
    savefig("../thesis_data/error_plots/" * name * ".html")
    plt
end

function save_results(name, results)
    params = Dict([k[1] => k[2][2] for k in results])
    f = open("../thesis_data/results/" * name * ".json", write=true)
    write(f, JSON.json(params))
    close(f)
end

function save_errors(name, errors)
    f = open("../thesis_data/results/" * name * "_errors.json", write=true)
    write(f, JSON.json(errors))
    close(f)
end

function save_raw_results(name, results)
    f = open("../thesis_data/results/" * name * ".json", write=true)
    write(f, JSON.json(results))
    close(f)
end

function load_results(name)
    f = open("../thesis_data/results/" * name * ".json")
    params = JSON.parse(read(f, String))
    close(f)
    params
end
