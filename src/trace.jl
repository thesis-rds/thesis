abstract type AbstractTrace end
abstract type AbstractRawTrace <: AbstractTrace end
abstract type AbstractFilteredTrace <: AbstractRawTrace end
abstract type AbstractAnnotatedTrace <: AbstractFilteredTrace end
abstract type AbstractDeconvolutedTrace <: AbstractAnnotatedTrace end
abstract type AbstractFinalTrace <: AbstractDeconvolutedTrace end
abstract type AbstractMinimizedTrace <: AbstractFinalTrace end
abstract type AbstractNormalizedTrace <: AbstractMinimizedTrace end


mutable struct RawTrace <: AbstractRawTrace
    name::String
    # Raw trace vector
    raw::Vector{Float64}
end

struct FilteredTrace <: AbstractFilteredTrace
    name::String
    raw::Vector{Float64}
    # Size of the window of the SavitzkyGolayFilter applied to the raw signal
    filter_preconv_width::Int64
    # Order of the polynomial of the SavitzkyGolayFilter applied to the raw signal
    filter_preconv_poly::Int64
    # Filtered trace
    filtered::Vector{Float64}

    # INNER CONSTRUCTORS
    # Upgrade from RawTrace
    FilteredTrace(raw::RawTrace, filter_preconv_width::Int64, filter_preconv_poly::Int64) =
        new(raw.name,
            raw.raw,
            filter_preconv_width, filter_preconv_poly,
            # Create filtered trace, pad edges with start & end value.
            [
                # Pad left
                [raw.raw[1] for _=1:filter_preconv_width];
                # Take inner part, trim faulty edges
                SavitzkyGolayFilter{filter_preconv_width, filter_preconv_poly}()(raw.raw)[(filter_preconv_width + 1):(end - filter_preconv_width)];
                # Pad right
                [raw.raw[end] for _=1:filter_preconv_width]
            ]
        )
    # Full (for data files)
    FilteredTrace(name::String, raw::Vector{Float64}, filter_preconv_width::Int64, filter_preconv_poly::Int64, filtered::Vector{Float64}) =
        new(name, raw, filter_preconv_width, filter_preconv_poly, filtered)
end

struct AnnotatedTrace <: AbstractAnnotatedTrace
    name::String
    raw::Vector{Float64}
    filter_preconv_width::Int64
    filter_preconv_poly::Int64
    filtered::Vector{Float64}
    # Baseline
    baseline::Float64
    # Bounds of the baseline determining region
    baseline_region::PointCollection
    # True peaks corresponding to filtered EPSP peaks (8 per trace!)
    peaks::PointCollection

    # INNER CONSTRUCTORS
    # Upgrade from FilteredTrace
    AnnotatedTrace(filtered::FilteredTrace, baseline::Float64, baseline_region::PointCollection, peaks::PointCollection) =
        new(filtered.name,
            filtered.raw,
            filtered.filter_preconv_width,
            filtered.filter_preconv_poly,
            filtered.filtered,
            baseline, baseline_region, peaks
        )
    # Auto-upgrade
    AnnotatedTrace(filtered::FilteredTrace, baseline_treshold::Float64, peak_window::Int64) =
            new(filtered.name,
                filtered.raw,
                filtered.filter_preconv_width,
                filtered.filter_preconv_poly,
                filtered.filtered,
                baseline(filtered.raw, baseline_treshold)[1], # baseline
                baseline_region(filtered.raw, baseline(filtered.raw, baseline_treshold)[2]), # baseline region
                peaks_pc(filtered.filtered, peak_window)
            )
    AnnotatedTrace(filtered::FilteredTrace, baseline_treshold::Float64, peak_window::Int64, peak_selector::Vector{Int64}) =
            (_b = baseline(filtered.raw, baseline_treshold);
             return new(filtered.name,
                filtered.raw,
                filtered.filter_preconv_width,
                filtered.filter_preconv_poly,
                filtered.filtered,
                _b[1], # baseline
                baseline_region(filtered.raw, _b[2]), # baseline region
                peaks_pc(filtered.filtered, peak_window)[peak_selector...]
            ))
    # Full (for data files)
    AnnotatedTrace(name::String, raw::Vector{Float64},
                  filter_preconv_width::Int64, filter_preconv_poly::Int64, filtered::Vector{Float64},
                  baseline::Float64, baseline_region::PointCollection, peaks::PointCollection) =
              new(name, raw,
                  filter_preconv_width, filter_preconv_poly, filtered,
                  baseline, baseline_region, peaks)
end

struct DeconvolutedTrace <: AbstractDeconvolutedTrace
    name::String
    raw::Vector{Float64}
    filter_preconv_width::Int64
    filter_preconv_poly::Int64
    filtered::Vector{Float64}
    baseline::Float64
    baseline_region::PointCollection
    peaks::PointCollection

    # Delay after peaks until start of region
    region_start::Int64
    # Width of regions
    region_width::Int64
    # Tau estimation range
    sweep::StepRangeLen
    # Estimated tau value
    τ::Float64
    # Time step
    Δt::Float64
    # Deconvoluted trace
    deconvoluted::Vector{Float64}
    # Tau sweeps
    τ_sweep::Vector{Float64}

    # INNER CONSTRUCTORS
    # Upgrade from AnnotatedTrace with known estimated tau
    DeconvolutedTrace(annotated::AnnotatedTrace, τ::Float64, Δt::Float64 = 0.00025) =
        new(annotated.name,
            annotated.raw,
            annotated.filter_preconv_width,
            annotated.filter_preconv_poly,
            annotated.filtered,
            annotated.baseline,
            annotated.baseline_region,
            annotated.peaks,
            0, 0, 0.0:0.0, τ, Δt, deconvolute(annotated.filtered, τ, Δt), Float64[]
        )
    # Upgrade from AnnotatedTrace, estimate tau with lms-region
    DeconvolutedTrace(annotated::AnnotatedTrace, region_start::Int64, region_width::Int64, sweep::StepRangeLen = 0.0001:0.0001:0.1, Δt::Float64 = 0.00025) =
        (all_τ = lms_sweep(deconvolute, annotated.filtered[region_start:min(length(annotated.filtered), region_start + region_width)] .- annotated.baseline, sweep, Δt);
         τ = sweep[argmin(all_τ)];
         return new(annotated.name,
            annotated.raw,
            annotated.filter_preconv_width,
            annotated.filter_preconv_poly,
            annotated.filtered,
            annotated.baseline,
            annotated.baseline_region,
            annotated.peaks,
            region_start, min(region_width, length(annotated.filtered) - region_start), sweep, τ, Δt, deconvolute(annotated.filtered, τ, Δt), all_τ
        ))
    # Full (for data files)
    DeconvolutedTrace(name::String, raw::Vector{Float64},
                  filter_preconv_width::Int64, filter_preconv_poly::Int64, filtered::Vector{Float64},
                  baseline::Float64, baseline_region::PointCollection, peaks::PointCollection,
                  region_start::Int64, region_width::Int64, sweep::StepRangeLen, τ::Float64, Δt::Float64, deconvoluted::Vector{Float64}, τ_sweep::Vector{Float64}) =
              new(name, raw,
                  filter_preconv_width, filter_preconv_poly, filtered,
                  baseline, baseline_region, peaks,
                  region_start, region_width, sweep, τ, Δt, deconvoluted, τ_sweep)
end

struct FinalTrace <: AbstractFinalTrace
    name::String
    raw::Vector{Float64}
    filter_preconv_width::Int64
    filter_preconv_poly::Int64
    filtered::Vector{Float64}
    baseline::Float64
    baseline_region::PointCollection
    peaks::PointCollection
    region_start::Int64
    region_width::Int64
    sweep::StepRangeLen
    τ::Float64
    Δt::Float64
    deconvoluted::Vector{Float64}
    τ_sweep::Vector{Float64}

    # SG width
    filter_deconv_width::Int64
    # SG poly
    filter_deconv_poly::Int64
    # Final filtered trace
    final::Vector{Float64}

    # INNER CONSTRUCTORS
    # Upgrade from DeconvolutedTrace
    FinalTrace(deconvoluted::DeconvolutedTrace, filter_deconv_width::Int64, filter_deconv_poly::Int64) =
        new(deconvoluted.name,
            deconvoluted.raw,
            deconvoluted.filter_preconv_width,
            deconvoluted.filter_preconv_poly,
            deconvoluted.filtered,
            deconvoluted.baseline,
            deconvoluted.baseline_region,
            deconvoluted.peaks,
            deconvoluted.region_start,
            deconvoluted.region_width,
            deconvoluted.sweep,
            deconvoluted.τ,
            deconvoluted.Δt,
            deconvoluted.deconvoluted,
            deconvoluted.τ_sweep, filter_deconv_width, filter_deconv_poly,
            [
                [deconvoluted.deconvoluted[1] for _=1:filter_deconv_width];
                SavitzkyGolayFilter{filter_deconv_width, filter_deconv_poly}()(deconvoluted.deconvoluted)[(filter_deconv_width + 1):(end - filter_deconv_width)];
                [deconvoluted.deconvoluted[end] for _=1:filter_deconv_width]
            ]
        )
    # Full (for data files)
    FinalTrace(name::String, raw::Vector{Float64},
               filter_preconv_width::Int64, filter_preconv_poly::Int64, filtered::Vector{Float64},
               baseline::Float64, baseline_region::PointCollection, peaks::PointCollection,
               region_start::Int64, region_width::Int64, sweep::StepRangeLen, τ::Float64, Δt::Float64, deconvoluted::Vector{Float64}, τ_sweep::Vector{Float64},
               filter_deconv_width::Int64, filter_deconv_poly::Int64, final::Vector{Float64}) =
           new(name, raw,
               filter_preconv_width, filter_preconv_poly, filtered,
               baseline, baseline_region, peaks,
               region_start, region_width, sweep, τ, Δt, deconvoluted, τ_sweep,
               filter_deconv_width, filter_deconv_poly, final
           )
end

mutable struct MinimizedTrace <: AbstractMinimizedTrace
    name::String
    τ::Float64
    Δt::Float64
    baseline::Float64
    # Input final trace
    final::Vector{Float64}
    # Peak amplitudes & times
    points::PointCollection
    MinimizedTrace(final::FinalTrace, final_peak_window::Int64, final_peak_selector::Vector{Int64} = Int64[]) =
        new(final.name, final.τ, final.Δt, final.baseline, final.final,
            length(final_peak_selector) == 0 ? peaks_pc(final.final, final_peak_window) :
            peaks_pc(final.final, final_peak_window)[final_peak_selector...]
        )
    MinimizedTrace(name::String, τ::Float64, Δt::Float64, baseline::Float64, final::Vector{Float64}, points::PointCollection) =
        new(name, τ, Δt, baseline, final, points)
end

struct NormalizedTrace <: AbstractNormalizedTrace
    name::String
    Δt::Float64
    # Normalized trace
    normal::Vector{Float64}
    # Normalized peak amplitudes & times
    points::PointCollection
end

function getTraceTypeString(trace)
    trace isa MinimizedTrace ? "minimized" :
    trace isa AbstractFinalTrace ? "final" :
    trace isa AbstractDeconvolutedTrace ? "deconvoluted" :
    trace isa AbstractAnnotatedTrace ? "annotated" :
    trace isa AbstractFilteredTrace ? "filtered" :
    trace isa AbstractRawTrace ? "raw" : "unknown"
end

function getTraceType(trace::String)
    trace == "raw" ? RawTrace :
    trace == "filtered" ? FilteredTrace :
    trace == "annotated" ? AnnotatedTrace :
    trace == "deconvoluted" ? DeconvolutedTrace :
    trace == "final" ? FinalTrace :
    trace == "minimized" ? MinimizedTrace : AbstractTrace
end

import Base.convert
convert(::Type{RawTrace}, x::AbstractRawTrace) = RawTrace(x.name, x.raw)
convert(::Type{FilteredTrace}, x::AbstractFilteredTrace) = FilteredTrace(x.name, x.raw, x.filter_preconv_width, x.filter_preconv_poly, x.filtered)
convert(::Type{AnnotatedTrace}, x::AbstractAnnotatedTrace) = AnnotatedTrace(x.name, x.raw, x.filter_preconv_width, x.filter_preconv_poly, x.filtered, x.baseline, x.baseline_region, x.peaks)
convert(::Type{DeconvolutedTrace}, x::AbstractDeconvolutedTrace) = DeconvolutedTrace(x.name, x.raw, x.filter_preconv_width, x.filter_preconv_poly, x.filtered, x.baseline, x.baseline_region, x.peaks, x.region_start, x.region_width, x.τ, x.deconvoluted)
