# Quick-use table		U		F		D
# Strong depression:    0.7		0.02	1.70
# Depression:		    0.5		0.05	0.50
# F-D:			        0.25	0.20	0.20
# Facilitation:		    0.15	0.50	0.05
# Strong facilitation:	0.1		1.70	0.02


function TM_ext(timestamps, α, A, U, τF, τD)
	PSP = Vector{Float64}(undef, length(timestamps))
    u = U
    R = 1.
	PSP[1] = A * R * u ^ α

    for i in 2:length(timestamps)
        δt = timestamps[i] - timestamps[i-1]
        eD = exp(-δt / τD)
        eF = exp(-δt / τF)
        R = (R * (1. - u ^ α) - 1) * eD + 1.
        u = u * eF + U * (1. - u * eF)
		# u = u * eF * (1 - U) + U
        PSP[i] = A * R * u ^ α
    end
    return PSP
end

function TM(timestamps, A, U, F, D)
	PSP = Vector{Float64}(undef, length(timestamps))
	PSP[1] = A * U
	u = U
	R = 1.

	for i = 2:length(timestamps)
		dt = timestamps[i] - timestamps[i - 1]
		R = (R * (1. - u) - 1.) * exp(-dt/D) + 1.
		u = u * (1. - U) * exp(-dt/F) + U
		PSP[i] = A * R * u
	end
	return PSP
end

function TM_cost(values, model_values)
	return sum((values .- model_values) .^ 2)
end
