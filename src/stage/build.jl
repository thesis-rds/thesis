"""
Build the trace's julia file.
"""
function build(trace::AbstractTrace)
    saveTrace(trace)
end

"""
Build a trace from its persistent metadata state.
"""
function build(t::Type{T}, name::String) where T <: AbstractTrace
    trace = from_meta(t, name)
    build(trace)
end
