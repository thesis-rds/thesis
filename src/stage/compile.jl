using JSON

function compile(trace::AbstractTrace)
    compile_json(trace)
end

"""
Compile a trace into a Plotly HTML file, adding plotly-cdn instead of local_provider (fixes _use_remote)
"""
function compile_html(trace::AbstractTrace)
    if trace isa AbstractNormalizedTrace
        dir = "normalized/"
    elseif trace isa AbstractMinimizedTrace
        dir = "minimized/"
    elseif trace isa AbstractFinalTrace
        dir = "final/"
    elseif trace isa AbstractDeconvolutedTrace
        dir = "deconvoluted/"
    elseif trace isa AbstractAnnotatedTrace
        dir = "annotated/"
    elseif trace isa AbstractFilteredTrace
        dir = "filtered/"
    elseif trace isa AbstractRawTrace
        dir = "raw/"
    end
    fname = Thesis.DIR_D * dir * "html/" * trace.name * ".html"
    plot()
    plotTrace(trace)
    savefig(fname)
    f = open(fname, "a+")
    seekstart(f)
    lines = readlines(f)
    lines[1] = replace(lines[1], "file://" => "https://cdn.plot.ly/plotly-latest.min.js")
    seekstart(f)
    [write(f, line * "\n") for line in lines]
    close(f)
    return fname, join(lines, "\n")
end

"""
Extract the Plotly JSON object from a compiled html file.
"""
function process_html(html::String)
    data_start = findfirst("PLOT,", html)[end] + 2
    data_end = findfirst("}]", html)[end]
    data = html[data_start:data_end]
    layout_start = data_end + 3
    layout_end = findfirst("});", html)[1]
    layout = html[layout_start:layout_end]
    json = "{\"data\":" * data * ", \"layout\": " * layout * "}"
    json_data = JSON.parse(json)
end

"""
Compile a JSON object as minimum represantation of this trace's Plotly plot.
"""
function compile_json(trace::AbstractTrace)
    fname_html, html_content = compile_html(trace)
    fname_json = replace(fname_html, "html" => "json")
    json = process_html(html_content)
    fj = open(fname_json, "w")
    write(fj, JSON.json(json))
    close(fj)
    json
end
