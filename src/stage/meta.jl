function get_all_metadata()
    f = open(DIR_D * "meta.json")
    json = JSON.parse(read(f, String))
    close(f)
    json
end

function store_all_metadata(meta)
    f = open(DIR_D * "meta.json", "w")
    json = JSON.json(meta)
    write(f, json)
    close(f)
    json
end

"""
Create a dictionary containing metadata keys from the trace.
"""
function extract_meta(trace::AbstractRawTrace)
    Dict{String,Any}(
        "name" => trace.name,
        "has" => ["raw"]
    )
end

function extract_meta(trace::AbstractFilteredTrace)
    Dict{String,Any}(
        "filter_preconv_width" => trace.filter_preconv_width,
        "filter_preconv_poly" => trace.filter_preconv_poly,
        "has" => ["raw", "filtered"]
    )
end

function drop(meta, names::String...)
    for name in names
        if !haskey(meta, name)
            meta[name]["curation"] = "dropped"
        end
    end
    meta
end

function restore(meta, curation::String, names::String...)
    for name in names
        if !haskey(meta, name)
            meta[name]["curation"] = curation
        end
    end
    meta
end


function update_meta!(meta, name::String, dict::Dict{String,Any})
    if !haskey(meta, name)
        meta[name] = Dict()
    end
    for key in keys(dict)
        meta[name][key] = dict[key]
    end
end

function from_meta(TraceType::Type{T}, name::String) where T <: AbstractTrace
    meta = get_all_metadata()
    if !haskey(meta, name)
        print("Trace with name '" * name * "' not found.")
        false
    else
        trace = loadRawTrace(name)
        if TraceType <: AbstractFilteredTrace
            trace = FilteredTrace(trace, meta[name]["filter_preconv_width"], meta[name]["filter_preconv_poly"])
        end
        if TraceType <: AbstractAnnotatedTrace
            if haskey(meta[name],"peaks_selector") && length(meta[name]["peaks_selector"]) > 0
                trace = AnnotatedTrace(trace, convert(Float64, meta[name]["baseline_treshold"]), meta[name]["peak_window"], convert(Vector{Int64},meta[name]["peaks_selector"]))
            else
                trace = AnnotatedTrace(trace, convert(Float64, meta[name]["baseline_treshold"]), meta[name]["peak_window"])
            end
        end
        if TraceType <: AbstractDeconvolutedTrace
            if haskey(meta[name],"tau") && (meta[name]["tau"] == 0. || meta[name]["tau"] == "")
                trace = DeconvolutedTrace(trace,
                    Int64(trace.peaks[end].X[1] + meta[name]["region_delay"]),
                    Int64(meta[name]["region_width"]),
                    eval(Meta.parse(meta[name]["sweep"])),
                    Float64(meta[name]["delta"])
                )
            else
                trace = DeconvolutedTrace(trace,
                    Float64(meta[name]["tau"]),
                    Float64(meta[name]["delta"])
                )
            end
        end
        if TraceType <: AbstractFinalTrace
            trace = FinalTrace(trace,
                Int64(meta[name]["filter_deconv_width"]),
                Int64(meta[name]["filter_deconv_poly"])
            )
        end
        if TraceType <: MinimizedTrace
            trace = from_meta(FinalTrace, name)
            trace = MinimizedTrace(trace, meta[name]["final_peak_window"], convert(Vector{Int64}, meta[name]["final_peak_selector"]))
        end
        trace
    end
end
