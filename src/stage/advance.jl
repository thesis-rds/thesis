function advance(::Type{FilteredTrace}, filter_width::Int64, filter_poly::Int64)
    loadAllRaw()
    meta = get_all_metadata()
    for trace in Thesis.DataRaw
        trace_meta = Dict()
        if haskey(meta, trace.name)
            trace_meta["filter_preconv_width"] = haskey(meta[trace.name], "filter_preconv_width") ? meta[trace.name]["filter_preconv_width"] : filter_width
            trace_meta["filter_preconv_poly"] = haskey(meta[trace.name], "filter_preconv_poly") ? meta[trace.name]["filter_preconv_poly"] : filter_poly
        else
            trace_meta = Dict("filter_preconv_width" => filter_width, "filter_preconv_poly" => filter_poly)
        end
        new_trace = advance(trace, trace_meta["filter_preconv_width"], trace_meta["filter_preconv_poly"])
        update_meta!(meta, trace.name, extract_meta(new_trace))
    end
    store_all_metadata(meta)
end

function advance(trace::RawTrace, filter_width::Int64, filter_poly::Int64)
    f = FilteredTrace(trace, filter_width, filter_poly)
    build(f)
    compile(f)
    f
end

function advance(::Type{AnnotatedTrace}, baseline_treshold::Float64, peak_window::Int64)
    loadAllFiltered()
    meta = get_all_metadata()
    for trace in Thesis.DataFiltered
        trace_meta = Dict("peaks_selector" => [], "baseline_treshold" => baseline_treshold, "peak_window" => peak_window, "has" => ["raw","filtered","annotated"])
        if haskey(meta, trace.name)
            trace_meta["baseline_treshold"] = haskey(meta[trace.name], "baseline_treshold") ? meta[trace.name]["baseline_treshold"] : baseline_treshold
            trace_meta["peaks_selector"] = haskey(meta[trace.name], "peaks_selector") ? meta[trace.name]["peaks_selector"] : []
            trace_meta["peak_window"] = haskey(meta[trace.name], "peak_window") ? meta[trace.name]["peak_window"] : peak_window
        end
        new_trace = advance(trace, trace_meta["baseline_treshold"], trace_meta["peak_window"], Vector{Int64}(trace_meta["peaks_selector"]))
        update_meta!(meta, trace.name, trace_meta)
    end
    store_all_metadata(meta)
end

function advance(trace::FilteredTrace, baseline_treshold::Float64, peak_window::Int64, peaks_selector::Vector{Int64} = Int64[])
    if length(peaks_selector) == 0
        a = AnnotatedTrace(trace, baseline_treshold, peak_window)
    else
        a = AnnotatedTrace(trace, baseline_treshold, peak_window, peaks_selector)
    end
    build(a)
    compile(a)
    a
end

function advance(::Type{DeconvolutedTrace}, region_delay::Int64, region_width::Int64)
    loadAllAnnotated()
    meta = get_all_metadata()
    i = 0
    total = length(Thesis.DataAnnotated)
    for trace in Thesis.DataAnnotated
        i = i + 1
        print("Advancing trace '" * trace.name * "' ($i/$total)\n")
        if meta[trace.name]["curation"] == "dropped"
            print("Skipping dropped trace '" * trace.name * "'\n")
            continue
        end
        trace_meta = Dict("region_delay" => region_delay,
                          "region_width" => region_width,
                          "delta" => 0.00025,
                          "tau" => 0.,
                          "sweep" => "0.0001:0.0001:0.1",
                          "has" => ["raw","filtered","annotated", "deconvoluted"])
        new_trace = advance(trace, trace_meta["region_delay"], trace_meta["region_width"], trace_meta["tau"], trace_meta["delta"])
        update_meta!(meta, trace.name, trace_meta)
    end
    store_all_metadata(meta)
end

function advance(trace::AnnotatedTrace, region_delay::Int64, region_width::Int64, τ::Float64, Δt::Float64)
    if τ != 0.
        d = DeconvolutedTrace(trace, τ, Δt)
    else
        d = DeconvolutedTrace(trace, Int64(trace.peaks[end].X[1] + region_delay), region_width)
    end
    build(d)
    compile(d)
    d
end

function advance(::Type{FinalTrace}, filter_deconv_width::Int64, filter_deconv_poly::Int64)
    loadAllDeconvoluted()
    meta = get_all_metadata()
    i = 0
    total = length(Thesis.DataDeconvoluted)
    for trace in Thesis.DataDeconvoluted
        i = i + 1
        print("Advancing trace '" * trace.name * "' ($i/$total)\n")
        if meta[trace.name]["curation"] == "dropped"
            print("Skipping dropped trace '" * trace.name * "'\n")
            continue
        end
        trace_meta = Dict("filter_deconv_width" => filter_deconv_width,
                          "filter_deconv_poly" => filter_deconv_poly,
                          "has" => ["raw","filtered","annotated", "deconvoluted", "final"])
        new_trace = advance(trace, trace_meta["filter_deconv_width"], trace_meta["filter_deconv_poly"])
        update_meta!(meta, trace.name, trace_meta)
    end
    store_all_metadata(meta)
end

function advance(trace::DeconvolutedTrace, filter_deconv_width::Int64, filter_deconv_poly::Int64)
    f = FinalTrace(trace, filter_deconv_width, filter_deconv_poly)
    build(f)
    compile(f)
    f
end

function advance(::Type{MinimizedTrace}, final_peak_window::Int64)
    loadAllFinal()
    meta = get_all_metadata()
    i = 0
    total = length(Thesis.DataFinal)
    for trace in Thesis.DataFinal
        i = i + 1
        print("Advancing trace '" * trace.name * "' ($i/$total)\n")
        if meta[trace.name]["curation"] == "dropped"
            print("Skipping dropped trace '" * trace.name * "'\n")
            continue
        end
        trace_meta = Dict("final_peak_window" => final_peak_window,
                          "final_peak_selector" => [],
                          "has" => ["raw","filtered","annotated", "deconvoluted", "final", "minimized"])
        new_trace = advance(trace, final_peak_window)
        update_meta!(meta, trace.name, trace_meta)
    end
    store_all_metadata(meta)
end

function advance(trace::FinalTrace, final_peak_window::Int64, final_peak_selector::Vector{Int64} = Int64[])
    m = MinimizedTrace(trace, final_peak_window, final_peak_selector)
    build(m)
    compile(m)
    m
end
