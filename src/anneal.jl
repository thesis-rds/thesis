function evolve(ii, model, E, E_best, p, p_best, dp_max, p_min, p_max, Temp, atten)::Tuple{Any,Any,Any,Any}
  i = 1;
  dp = dp_max[ii] * ( 2. * rand() - 1.) / atten   # A random step (abs<dp_max[ii]) is attempted..
  # Reroll max 1000 times if the random step is not between p_max and p_min.
  while i < 1000 && ((p[ii] + dp < p_min[ii]) || (p[ii] + dp > p_max[ii]))
    dp      = dp_max[ii] * ( 1. - 2 * rand() ) / atten
    i = i + 1
  end
  p[ii]   = p[ii] + dp                                      # When the step is ok, it is performed, so that
  tmp     = model(p)
  if any(isnan(tmp)) || any(isinf(tmp))
    return E, E_best, p, p_best
  end

  dE = E - tmp                                # the *descrease* in the cost is evaluated..
  if (dE > 0.)                                # If such a change in p(i) is convenient,
    E = E - dE                                # we accept the new value for the p(i) and exit.
    if (E < E_best)                            # If this is even the best-ever score up to now,
      p_best = p                               # we write down the corresponding parameters..
      # print("****** E_best improved [" * string(E_best) * " -> " * string(E) * "]\n"); # and we let the user know it.
      E_best = E                               # The new 'best-ever' is set here.
    end
  else                                        # When the step is not favorable (i.e. dE < 0)..
    if rand() <= exp(dE / Temp)                # let's *maybe* keep the change anyway (tossing a coin)..
      E = E - dE                               # (this is the essence of the METROPOLIS algorithm)
    else
      p[ii] = p[ii] - dp                       # Otherwise, discard the step.
    end
  end
  E, E_best, p, p_best
end

function annealing_schedule(Temp, Temp_step, atten, atten_step)
  # At each time step (i.e. each call) the temperature is lowered..
  # and the attenuation is increased.
  Temp * Temp_step, atten * atten_step
end

function anneal(model, p, dp_max, p_min, p_max, MAXITER)::Tuple{Float64,Vector{Float64}}
  #
  #  ANNEAL Multidimensional constrained nonlinear minimization (Metropolis).
  # [E_best p_best] = anneal(model,p,dp_max,p_min,p_max,MAXITER)
  #
  # p      : Vector containing the current optimization parameters..
  # dp_max : Maximal allowed modification for each parameter..
  # p_max  : Allowed range for each parameter..
  # p_min  : Allowed range for each parameter..
  #
  #
  # p_best : Vector containing the 'best-ever' pars (i.e. corresponding to E_best)..
  # E_best : Best ever value of the cost function
  #
  #   Reference: Kirckpatric S., ...
  #
  #   Copyright 2006 Michele Giugliano, PhD
  #   Edited for Julia 1.0 by Robin De Schepper 2019 (also added model & cost factories)
  #   $Revision: 2.00 $  $Date: 2019/03/19 16:49:12 $

  p_best = p                # Vector containing the 'best-ever' pars (i.e. corresponding to E_best)..
  t = time()
  E = model(p)              # Current value of the cost function (i.e. the current energy)..
  t = time() - t
  if t == 0.
    t = time()
    for i = 0:10000
      E = model(p)
    end
    t = time() - t
  end
  E_best = E;                # Best ever value of the cost function ('fake' initializatio to 1.e20)..
  E_stuck_test    = 0.;      # Variable storing the value of the cost, when stuck in a local minimum..

  # print("starting from " * string(E) * " (" * string(round(t / 10., sigdigits = 4)) * " ms to evaluate your function)\n");

  #------------------------------------------------------------------------------------------------
  #------------------------------------------------------------------------------------------------
  ii              = 1;       # Counter on the parameter number..
  iter            = 2;       # Index counting iterations (it starts from 2)..
  Npar            = prod(size(p));# Total no of *free* optimization parameters..
  freeze          = false;       # Boolean variable associated to high temperature "freezing".
  Temp_init       = 40.;      # Initial value of the absolute 'temperature', for the annealing schedule..
  Temp            = Temp_init;# Current value of the absolute 'temperature', for the annealing schedule..
  Temp_step       = 0.99;    # 'Temperature' decrease step factor, for the annealing schedule..
  atten_init      = 1.;      # Initial value of the 'attenuation'..
  atten           = atten_init;# Current value of the 'attenuation'..
  atten_step      = 1.001;   # 'Attenuation' increase step factor, for the annealing schedule..

  stuck_time      = 0;       # Variable counting the number of iterations, while stuck in a local minumum,.
  max_stuck_times = 2000;    # Max number of times to be stucked in a local minimum..
  restart         = 0;       # Variable storing the number of attempts at 'recoverying' from a local minimum..
  max_restarts    = 10;      # Max number of times the process attempt at 'recoverying' from a local minimum..
  max_iter        = 100000;  # Max overall number of iterations to be performed [UNUSED].
  quake           = 100000;  # No of iterations after which an "earthquake" occurs.
  quakes          = 2;       # Var storing the no of iterations before a minor 'earthquake'.

  #MAXITER         = 100;     # Maximal number of allowed function iterations..
  #------------------------------------------------------------------------------------------------
  #------------------------------------------------------------------------------------------------

  while(iter < MAXITER)                               # This is the main loop for the simulated annealing.
    iter = iter + 1;
    if iter % 1000 == 0
      # print(string(iter) * " iterations so far..\n")
    end
    #---------
    if !freeze                                       # The 'temperature' is constantly decreased (for the next step)
      Temp, atten = annealing_schedule(Temp, Temp_step, atten, atten_step);
    end
    #---------
    if (abs(E_stuck_test - E) < 0.001)                 # If the parameters are stuck in a local minimum..
      stuck_time = stuck_time + 1;                      # then the change in the energy level is very small.
    else                                               # It is therefore useful to capture such a conditions,
      E_stuck_test = E;                                 # and when the number of stucked iterations is larger enough..
    end
    #---------
    if (stuck_time >= max_stuck_times)                  # It provides a 'temper. remescio' (jargon) to allow the minimum,
      Temp  = Temp_init  * 0.5;                          # to be somehow overcome. Of course, at such a stage, all the
      atten = atten_init * 0.5;                          # (the 'atten' factor is also released a little bit).
      stuck_time = 0; restart = restart + 1;       # counters must be reset to zero to start again the check.
    end
    #---------
    if (restart == max_restarts)                       # However, after some time and after a given number of restarts
      Temp = Temp_init;       restart = 0;          # like those reported above, it is useful to have a large tempe-
      atten= atten_init;                            # rature outbreak as it was in the very beginning.
    end
    #---------
    if quakes % quake == 0
      ii = rand(1:Npar)                        # A parameter is chosen by chance..
      dp      =  dp_max[ii] * ( 1. - 2 * rand() ) / atten   # A random step (abs<dp_max[ii]) is attempted..
      while (((p[ii]+dp) < p_min[ii]) | ((p[ii]+dp) > p_max[ii])) # Is the random step allowed ?
        dp      = dp_max[ii] * ( 1. - 2 * rand() ) / atten           # Otherwise, let's find another step.
      end
      p[ii]   = p[ii] + dp                                       # When the step is ok, it is performed, so that
      quakes = 0
    end
    #---------
    # Let's finally perform an evolution step..
    E, E_best, p, p_best = evolve(ii, model, E, E_best, p, p_best, dp_max, p_min, p_max, Temp, atten)
    ii = max((ii + 1) % (Npar + 1), 1)
    quakes  = quakes + 1;
  end
  E_best, p_best
end

function tm_model_factory(model, cost, data...)
  # Returns a function that can be called with model(params) where params is a parameter array
  # that will expand into model(data[0], data[1], data[n], params[0], params[1], params[n])
  # and returns the result of a cost function applied to the result of the model with those parameters.
  return function(params)
    cost(model(data..., params...))
  end
end

function tm_cost_factory(cost, values)
  # Returns a function that calls the cost function to compare values with model_values.
  return function(model_values)
    cost(values, model_values)
  end
end

function tm_model_multi_factory(model_multi)
  return function(params)
    error_sum = 0.
    for i = 1:length(model_multi)
      error_sum = error_sum + model_multi[i](params)
    end
    return error_sum
  end
end

function tm_model_group_factory(group, cost, α = 1)
  if α == 0
    return function(params)
      return cost(group, params)
    end
  else
    return function(params)
      return cost(group, params, α)
    end
  end
end

function brute_force(model, ranges)
  p = [ range[1] for range in ranges]
  E = model(p)
  E, p = brute_force_dimension(model, p, ranges, 1, E, p)
end

function brute_force_dimension(model, p, ranges, param_index, best_E, best_P)
  if param_index < length(p)
    for i in ranges[param_index]
      p[param_index] = i
      best_E, best_P = brute_force_dimension(model, p, ranges, param_index + 1, best_E, best_P)
    end
  else
    for i in  ranges[param_index]
      p[param_index] = i
      E = model(p)
      if E < best_E
        best_P = copy(p)
        best_E = copy(E)
      end
    end
  end
  best_E, best_P
end
