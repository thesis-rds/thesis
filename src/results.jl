using Main.Thesis, Plots, JSON, SharedArrays, Distributed
plotly()

function quadratic(values, model_values)
    sum((values .- model_values) .^ 2)
end

function multistart(model, p_min, p_max, iter = 100)
    best_E = typemax(Float64)
    errors = SharedArray{Float64}(iter)
    parameters = [SharedArray{Float64}(length(p_min)) for _ = 1:iter]
    @sync @distributed for i in 1:iter
        p = rand() .* (p_max .- p_min) .+ p_min
        E, p = anneal(model, p, p ./ 5., p_min, p_max, 100000)
        errors[i] = E
        for j = 1:length(p_min)
            parameters[i][j] = p[j]
        end
    end
    best_p = parameters[argmin(errors)]
    best_p
end

function create_results(name, traces, α = 1)
    results = Dict()
    meta = get_all_metadata()
    errors = Dict()
    try
        mkdir("../thesis_data/result_plots/" * name)
        mkdir("../thesis_data/reconstr_plots/" * name)
    catch
    end
    for trace_m in traces
        if meta[trace_m.name]["curation"] == "minimized"
            print("annealing ", trace_m.name, "\n")
            # Set cost function to quadratic and set recorded values to compare with.
            cost = tm_cost_factory(quadratic, trace_m.points.Y .- trace_m.baseline)
            # Prepend `timestamps` and `α` to model parameters.
            model = tm_model_factory(TM_ext, cost, trace_m.points.X .* trace_m.Δt, α)
            res = multistart(model, [0.0, 0.0, 0.0, 0.0], [200., 1.0, 2.0, 2.0], 200)
            model_peaks = TM_ext(trace_m.points.X .* trace_m.Δt, α, res...)
            plot(); plotTrace(trace_m); scatter!(trace_m.points.X, model_peaks .+ trace_m.baseline, markersize=3)
            savefig("../thesis_data/result_plots/" * name * "/" * trace_m.name * ".html")
            results[trace_m.name] = (trace_m, res)
            reconv_original = reconvolute(peak_epsp(trace_m.points.X .* trace_m.Δt, trace_m.points.Y .- trace_m.baseline, 1.0, trace_m.Δt), trace_m.τ, trace_m.Δt, 0.);
            reconv_model = reconvolute(peak_epsp(trace_m.points.X .* trace_m.Δt, model_peaks, 1.0, trace_m.Δt), trace_m.τ, trace_m.Δt, 0.);
            plot(); plot!(reconv_original); plot!(reconv_model);
            savefig("../thesis_data/reconstr_plots/" * name * "/" * trace_m.name * ".html")
            errors[trace_m.name] = sum((reconv_original .- reconv_model) .^ 2)
            save_results(name, results)
        end
    end
    plot(); scatter(1:length(errors), collect(values(errors)), hover = collect(keys(errors)))
    savefig("../thesis_data/error_plots/" * name * ".html")
    save_results(name, results)
    results, errors
end

struct TraceGroup
    name::String
    traces::Vector{AbstractTrace}
    param_range::Matrix{Float64}
end

function create_grouped_results(name, groups, α = 1)
    results = Dict()
    meta = get_all_metadata()
    errors = Dict()
    try
        mkdir("../thesis_data/result_plots/" * name)
    catch
    end
    for group in groups
        group_count = 0
        multi_model = []
        for trace_m in group.traces
            if meta[trace_m.name]["curation"] == "minimized"
                group_count = group_count + 1
                # Set cost function to quadratic and set recorded values to compare with.
                cost = tm_cost_factory(quadratic, trace_m.points.Y .- trace_m.baseline)
                # Prepend `timestamps` and `α` to model parameters.
                push!(multi_model, tm_model_factory(TM_ext, cost, trace_m.points.X .* trace_m.Δt, α))
            end
        end
        if group_count > 0
            old_err = typemax(Float64)
            old_params = Nothing
            try
                mkdir("../thesis_data/result_plots/" * name * "/" * group.name)
            catch
            end
            for group_repeats = 1:3
                model = tm_model_multi_factory(multi_model)
                print("annealing ", group.name, "\n")
                res = multistart(model, group.param_range[:, 1], group.param_range[:, 2], 200)
                sum_error = 0.
                weight = 0.
                for trace_m in group.traces
                    model_peaks = TM_ext(trace_m.points.X .* trace_m.Δt, α, res...)
                    sum_error = sum_error + sum((trace_m.points.Y .- trace_m.baseline .- model_peaks) .^ 2)
                    weight = weight + (maximum(trace_m.points.Y .- trace_m.baseline) * length(trace_m.points.X))
                end
                sum_error = sum_error / weight
                if sum_error < old_err
                    print("** new err:", sum_error, "\nold err:", old_err, "\n")
                    for trace_m in group.traces
                        model_peaks = TM_ext(trace_m.points.X .* trace_m.Δt, α, res...)
                        plot(); plotTrace(trace_m); scatter!(trace_m.points.X, model_peaks .+ trace_m.baseline, markersize=3)
                        if old_params != Nothing
                            old_peaks = TM_ext(trace_m.points.X .* trace_m.Δt, α, res...)
                            scatter!(trace_m.points.X, old_peaks .+ trace_m.baseline, markersize=3, opacity=0.4)
                        end
                        savefig("../thesis_data/result_plots/" * name * "/" * group.name * "/" * trace_m.name * ".html")
                    end
                    old_params = res
                    old_err = sum_error
                    results[group.name] = res
                    errors[group.name] = sum_error
                    save_raw_results(name, results)
                end
            end
        end
    end
    plot(); scatter(1:length(errors), collect(values(errors)), hover = collect(keys(errors)))
    savefig("../thesis_data/error_plots/" * name * "_norm.html")
    save_raw_results(name, results)
    results, errors
end

function create_errors_from_results(name, normalized = false, α = 1)
    results = load_results(name)
    errors = Dict()
    for res in results
        trace_m = loadMinimizedTrace(res[1])
        model_peaks = TM_ext(trace_m.points.X .* trace_m.Δt, α, res[2]...)
        reconv_original = reconvolute(peak_epsp(trace_m.points.X .* trace_m.Δt, trace_m.points.Y .- trace_m.baseline, 1.0, trace_m.Δt), trace_m.τ, trace_m.Δt, 0.);
        reconv_model = reconvolute(peak_epsp(trace_m.points.X .* trace_m.Δt, model_peaks, 1.0, trace_m.Δt), trace_m.τ, trace_m.Δt, 0.);
        normalization = normalized ? maximum(trace_m.points.Y .- trace_m.baseline) * length(trace_m.points.X) : 1.
        errors[trace_m.name] = sqrt(sum((reconv_original .- reconv_model) .^ 2)) / normalization
    end
    plot_errors(name * (normalized ? "_norm" : ""), errors)
    errors
end

function plot_errors(name, errors = Nothing)
    if errors == Nothing
        f = open("../thesis_data/results/" * name * ".json", read = true)
        errors = JSON.parse(read(f, String))
        close(f)
    end
    plt = scatter(1:length(errors), collect(values(errors)), hover = collect(keys(errors)))
    savefig("../thesis_data/error_plots/" * name * ".html")
    plt
end

function save_results(name, results)
    params = Dict([k[1] => k[2][2] for k in results])
    f = open("../thesis_data/results/" * name * ".json", write=true)
    write(f, JSON.json(params))
    close(f)
end

function save_errors(name, errors)
    f = open("../thesis_data/results/" * name * "_errors.json", write=true)
    write(f, JSON.json(errors))
    close(f)
end

function save_raw_results(name, results)
    f = open("../thesis_data/results/" * name * ".json", write=true)
    write(f, JSON.json(results))
    close(f)
end

function load_results(name)
    f = open("../thesis_data/results/" * name * ".json")
    params = JSON.parse(read(f, String))
    close(f)
    params
end

the_list = ["p2idep35h20", "p2idep25h30", "p2idep24h20", "p2idep54h20", "p2idep35h"]
function improve_results(name, traces, α = 1)
    results = load_results(name)
    for i in 1:5
        for trace_m in traces
            if !haskey(results, trace_m.name)
                continue
            end
            old_result = results[trace_m.name]
            print("annealing ", trace_m.name, "\n")
            # Set cost function to quadratic and set recorded values to compare with.
            cost = tm_cost_factory(quadratic, trace_m.points.Y .- trace_m.baseline)
            # Prepend `timestamps` and `α` to model parameters.
            model = tm_model_factory(TM_ext, cost, trace_m.points.X .* trace_m.Δt, α)
            res = multistart(model, [0.0, 0.0, 0.0, 0.0], [500., 1.0, 4.0, 4.0], 500)
            model_peaks = TM_ext(trace_m.points.X .* trace_m.Δt, α, res...)
            old_model_peaks = TM_ext(trace_m.points.X .* trace_m.Δt, α, old_result...)
            reconv_original = reconvolute(peak_epsp(trace_m.points.X .* trace_m.Δt, trace_m.points.Y .- trace_m.baseline, 1.0, trace_m.Δt), trace_m.τ, trace_m.Δt, 0.);
            reconv_model = reconvolute(peak_epsp(trace_m.points.X .* trace_m.Δt, model_peaks, 1.0, trace_m.Δt), trace_m.τ, trace_m.Δt, 0.);
            reconv_old_model = reconvolute(peak_epsp(trace_m.points.X .* trace_m.Δt, old_model_peaks, 1.0, trace_m.Δt), trace_m.τ, trace_m.Δt, 0.);
            err = sum((reconv_original .- reconv_model) .^ 2)
            old_err = sum((reconv_original .- reconv_old_model) .^ 2)
            if err < old_err
                print("** new err:", err, "\nold err:", old_err, "\n")
                plot(); plotTrace(trace_m); scatter!(trace_m.points.X, model_peaks .+ trace_m.baseline, markersize=3)
                savefig("../thesis_data/result_plots/" * trace_m.name * ".html")
                plot(reconv_original); plot!(reconv_model);
                savefig("../thesis_data/reconstr_plots/" * trace_m.name * ".html")
                results[trace_m.name] = res
                save_raw_results(name, results)
            end
        end
    end
end
