function plotTrace(trace::AbstractRawTrace)
    plot!(trace.raw, seriescolor = RGBA(1, 0, 0))
end

function plotTrace(trace::AbstractFilteredTrace)
    plot!(trace.raw, seriescolor = RGBA(1, 0, 0, 0.2), linewidth = 4)
    plot!(trace.filtered, seriescolor = RGBA(1, 0, 0))
end

rect(X, Y) = Shape([X[1], X[1], X[2], X[2]], [Y[1], Y[2], Y[2], Y[1]])

function plotTrace(trace::AbstractAnnotatedTrace)
    filtered = convert(FilteredTrace, trace)
    plotTrace(filtered)
    scatter!(trace.peaks.X, trace.peaks.Y, markershape = :circle, fillcolor = :lime, markersize = 3)
    plot!(rect(trace.baseline_region.X, trace.baseline_region.Y), opacity = 0.5, linecolor = false, fillcolor = :lime)
    plot!([1, length(trace.raw)], [trace.baseline, trace.baseline], linewidth=3, linecolor = "#329125")
end

function plotTrace(trace::AbstractDeconvolutedTrace)
    filtered = convert(FilteredTrace, trace)
    plot!(trace.deconvoluted, seriescolor = RGBA(1, 1, 0), linewidth = 2,
        xguide = "Time (ms)", xformatter = x -> round(Int64, x * trace.Δt * 1000),
        yguide = "Postsynaptic membrane potential (mV)"
    )
    plt = plot!(filtered.filtered, seriescolor = RGBA(1, 0, 0), linewidth = 1)
    if(trace.region_start != 0)
        plot!(rect(
              [trace.region_start, trace.region_start + trace.region_width], #region X's
              [trace.filtered[trace.region_start], trace.filtered[trace.region_start + trace.region_width]] #region Y's
            ), opacity = 0.5, linecolor = false, fillcolor = :blue)
    end
    if(length(trace.τ_sweep) != 0)
        plot!(
            trace.τ_sweep,
            inset = (plt, bbox(0,0,0.5,0.25,:top,:right)),
            subplot=2,
            bg_inside=nothing,
            legend=:none,
            yaxis = false,
            xformatter = x -> round(Int64, x * ((trace.sweep)[2] - (trace.sweep)[1]) * 1000),
            xguide = "τ (ms)",
            yguide = "Sum of squares"
        )
        scatter!([argmin(trace.τ_sweep)], [minimum(trace.τ_sweep)], hover=[string(round(trace.τ * 1000, digits=2)) * "ms"], subplot=2)
    end
    plt
end

function plotTrace(trace::AbstractFinalTrace)
    plot!(trace.deconvoluted, seriescolor = RGBA(0.541, 0.937, 0.462), linewidth = 2,
        xguide = "Time (ms)", xformatter = x -> round(Int64, x * trace.Δt * 1000),
        yguide = "Postsynaptic membrane potential (mV)"
    )
    plot!(trace.final, seriescolor = RGBA(0.172, 0.513, 0.105), linewidth = 1)
    plot!(trace.filtered, seriescolor = RGBA(1, 0, 0), linewidth = 1)
end

function plotTrace(trace::AbstractMinimizedTrace)
    plot!(trace.final, seriescolor = RGBA(0.172, 0.513, 0.105), linewidth = 1,
        xguide = "Time (ms)", xformatter = x -> round(Int64, x * trace.Δt * 1000),
        yguide = "Deconvoluted membrane potential (mV)"
    )
    scatter!(trace.points.X, trace.points.Y, markersize = 2)
end
