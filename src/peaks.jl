import Statistics: mean

struct PointCollection
    X::Vector{Float64}
    Y::Vector{Float64}
end

import Base.getindex
function getindex(v::PointCollection, i::Int64...)
  PointCollection(v.X[[i...]], v.Y[[i...]])
end

import Base.lastindex
function lastindex(v::PointCollection)
  length(v.X)
end

import Base.push!
function push!(v::PointCollection, p::Tuple{Int64,Float64})
  push!(v.X, p[1])
  push!(v.Y, p[2])
end


"""
An algortihm to find peaks in a trace. Uses a sliding window of width `width * 2 + 1`
"""
function peaks(trace::Vector{Float64}, width::Int64)
  res = Int64[]
  for i = 1:length(trace)
    l = max(i - width, 1)
    r = min(i + width, length(trace))
    # Is this point equal to the maximum value in the window, and is it the only point with this value in the window? => is a peak
    if(trace[i] == maximum(trace[l:r]) && sum(trace[l:r] .== maximum(trace[l:r])) == 1)
      push!(res, i)
    end
  end
  res
end

"""
Performs `peaks` but returns a PointCollection
"""
function peaks_pc(trace::Vector{Float64}, width::Int64)::PointCollection
  x = peaks(trace, width)
  PointCollection(x, trace[x])
end

"""
Find baseline averaging pre-treshold area
"""
function baseline(trace::Vector{Float64}, treshold::Float64)
  if maximum(trace) < treshold
    return mean(trace), length(trace)
  end
  region_end = findall(trace .>= treshold)[1]
  mean(trace[1:region_end]), region_end
end

"""
Return the full baseline determining region
"""
function baseline_region(trace::Vector{Float64}, region_end::Int64)::PointCollection
  PointCollection([1, region_end], [minimum(trace[1:region_end]), maximum(trace[1:region_end])])
end
