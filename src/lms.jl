"""
	lms_sweep(f, input, sweep, Δt)

Performs a least mean square sweep of `f(Array{Number}, x, Δt)` and returns all the results
in an array.
"""
function lms_sweep(f, input, sweep, Δt)
	[sum( f(input, i, Δt) .^ 2) for i in sweep]
end

"""
	lms(f, input, sweep, Δt)

Performs a least mean square sweep of `f(Array{Number}, x, Δt)` and returns the value of the
parameter for the run with the smallest square.
"""
function lms(f, input, sweep, Δt)
	argmin(lms_sweep(f, input, sweep, Δt)) * (sweep[2] - sweep[1]) + sweep[1]
end
