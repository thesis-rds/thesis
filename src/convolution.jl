"""
	deconvolute(x, τ, Δt)

Perform τ * dx/Δt + x
"""
function deconvolute(x::Vector{Float64}, τ, Δt)
	[τ * (x[i+1] - x[i]) / Δt + x[i] for i = 1:(length(x) - 1)]
end

function reconvolute(x::Vector{Float64}, τ, Δt, initial = 0.)
	res = zeros(length(x))
	res[1] = initial
	for i in 1:(length(x) - 1)
		res[i + 1] = (x[i] - res[i]) * Δt / τ + res[i]
	end
	res
end
