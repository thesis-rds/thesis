"""
    poisson_until(λ, t)

Returns an Array of Poisson distributed random timestamps between 0 and t
with event rate λ
"""
function poisson_until(λ, t)
    u = Array{Float64,1}(undef, 0);
    d = Exponential(1 / λ);
    _t = rand(d);
    while _t < t
        push!(u, _t);
        _t += rand(d);
    end
    u
end
