using Main.Thesis, Plots, JSON, SharedArrays, Distributed
include("./gammq.jl")
plotly()

function chisq(values, model_values)
    sum((values .- model_values) .^ 2)
end

function multistart(model, p_min, p_max, iter = 100)
    best_E = typemax(Float64)
    errors = SharedArray{Float64}(iter)
    parameters = [SharedArray{Float64}(length(p_min)) for _ = 1:iter]
    @sync @distributed for i in 1:iter
        p = rand() .* (p_max .- p_min) .+ p_min
        E, p = anneal(model, p, p ./ 5., p_min, p_max, 100000)
        errors[i] = E
        for j = 1:length(p_min)
            parameters[i][j] = p[j]
        end
    end
    best_p = parameters[argmin(errors)]
    best_p
end

struct TraceGroup
    name::String
    traces::Vector{AbstractTrace}
    param_range::Matrix{Float64}
    class::String
end

struct GroupResult
    name::String
    class::String
    traces::Vector{AbstractTrace}
    parameters::Vector{Float64}
    x2::Float64
    q::Float64
end

"""Returns chi square of TM_ext for the given parameters and TraceGroup"""
function get_group_error(group::TraceGroup, parameters, α)
    sum_error = 0.
    for trace_m in group.traces
        expected = TM_ext(trace_m.points.X .* trace_m.Δt, α, parameters...)
        observed = trace_m.points.Y .- trace_m.baseline
        sum_error = sum_error + sum(((observed .- expected) .^ 2) ./ expected)
    end
    sum_error
end

"""Returns chi square of TM_ext for the given parameters and TraceGroup with α as a free parameter"""
function get_group_error(group::TraceGroup, parameters)
    sum_error = 0.
    for trace_m in group.traces
        expected = TM_ext(trace_m.points.X .* trace_m.Δt, parameters...)
        observed = trace_m.points.Y .- trace_m.baseline
        sum_error = sum_error + sum(((observed .- expected) .^ 2) ./ expected)
    end
    sum_error
end

function get_result_error(result::GroupResult)
    sum_error = 0.
    for trace_m in result.traces
        expected = TM_ext(trace_m.points.X .* trace_m.Δt, parameters...)
        observed = trace_m.points.Y .- trace_m.baseline
        sum_error = sum_error + sum(((observed .- expected) .^ 2) ./ expected)
    end
    sum_error
end

function get_results_X2(groups, results, α = 1)
    x2 = Dict()
    for group in groups
        if haskey(results, group.name)
            x2[group.name] = get_group_error(group, results[group.name], α)
        end
    end
    x2
end

function get_results_Q(groups, results)
    Q = Dict()
    for group in groups
        if haskey(results, group.name)
            x2 = get_group_error(group, results[group.name])
            N = sum([length(trace.points.Y) for trace in group.traces])
            M = 4
            Q[group.name] = gammq(0.5 * x2, 0.5 * (N - M))
        end
    end
    Q
end

function get_group_x2q(group::TraceGroup, parameters::Vector{Float64})
    x2 = get_group_error(group, parameters)
    N = sum([length(trace.points.Y) for trace in group.traces])
    M = 4
    x2, gammq(0.5 * x2, 0.5 * (N - M))
end

function create_grouped_results(name, groups, α = 0)
    meta = get_all_metadata()
    if isfile("../thesis_data/results/" * name * ".json")
        results = load_results(name)
    else
        results = Dict()
    end
    errors = Dict()
    for kv in results
        errors[kv[1]] = get_group_error(filter(x -> x.name == kv[1], groups)[1], kv[2], α)
    end
    try
        mkdir("../thesis_data/result_plots/" * name)
    catch
    end
    for group in groups
        group_count = 0
        multi_model = []
        for trace_m in group.traces
            if meta[trace_m.name]["curation"] == "minimized"
                group_count = group_count + 1
            end
        end
        if group_count > 0
            old_err = haskey(errors, group.name) ? errors[group.name] : typemax(Float64)
            old_params = Nothing
            try
                mkdir("../thesis_data/result_plots/" * name * "/" * group.name)
            catch
            end
            for group_repeats = 1:7
                model = tm_model_group_factory(group, get_group_error, α)
                print("annealing ", group.name, " (α = ", (α == 0 ? "free" : α), ")\n")
                if α == 0
                    res = multistart(model, group.param_range[:, 1], group.param_range[:, 2], 200)
                else
                    res = vcat([α], multistart(model, group.param_range[2:end, 1], group.param_range[2:end, 2], 200))
                end
                sum_error = get_group_error(group, res)
                if sum_error < old_err
                    print("** new err:", sum_error, "\nold err:", old_err, "\n")
                    for trace_m in group.traces
                        model_peaks = TM_ext(trace_m.points.X .* trace_m.Δt, res...)
                        plot(); plotTrace(trace_m); scatter!(trace_m.points.X, model_peaks .+ trace_m.baseline, markersize=3)
                        if old_params != Nothing
                            old_peaks = TM_ext(trace_m.points.X .* trace_m.Δt, res...)
                            scatter!(trace_m.points.X, old_peaks .+ trace_m.baseline, markersize=3, opacity=0.4)
                        end
                        savefig("../thesis_data/result_plots/" * name * "/" * group.name * "/" * trace_m.name * ".html")
                    end
                    old_params = res
                    old_err = sum_error
                    results[group.name] = res
                    errors[group.name] = sum_error
                    save_raw_results(name, results)
                end
            end
        end
    end
    plot(); scatter(1:length(errors), collect(values(errors)), hover = collect(keys(errors)))
    savefig("../thesis_data/error_plots/" * name * "_norm.html")
    save_raw_results(name, results)
    results, errors
end

function plot_errors(name, errors = Nothing)
    if errors == Nothing
        f = open("../thesis_data/results/" * name * ".json", read = true)
        errors = JSON.parse(read(f, String))
        close(f)
    end
    sorted_errors = sort(errors, by = name -> errors[name])
    plt = scatter(1:length(sorted_errors), collect(values(sorted_errors)), hover = collect(keys(sorted_errors)))
    savefig("../thesis_data/error_plots/" * name * ".html")
    plt
end

function plot_compare_errors(name, errors...)
    plt = plot();
    for err in errors
        sorted_errors = sort(err, by = name -> errors[1][name])
        plt = scatter!(1:length(sorted_errors), collect(values(sorted_errors)), hover = collect(keys(sorted_errors)))
    end
    savefig("../thesis_data/error_plots/" * name * ".html")
    plt
end

function save_results(name, results)
    params = Dict([k[1] => k[2][2] for k in results])
    f = open("../thesis_data/results/" * name * ".json", write=true)
    write(f, JSON.json(params))
    close(f)
end

function save_errors(name, errors)
    f = open("../thesis_data/results/" * name * "_errors.json", write=true)
    write(f, JSON.json(errors))
    close(f)
end

function save_raw_results(name, results)
    f = open("../thesis_data/results/" * name * ".json", write=true)
    write(f, JSON.json(results))
    close(f)
end

function load_results(name::String)
    f = open("../thesis_data/results/" * name * ".json")
    params = JSON.parse(read(f, String))
    close(f)
    params
end

function load_results(groups, name::String)
    f = open("../thesis_data/results/" * name * ".json")
    params = JSON.parse(read(f, String))
    close(f)
    [
        (
            group = filter(x -> x.name == name, groups)[1];
            GroupResult(name, group.class, group.traces, params[name], get_group_x2q(group, Vector{Float64}(params[name]))...)
        ) for name = keys(params)
    ]
end
