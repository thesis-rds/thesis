using DotEnv
DotEnv.config()
DIR_D = ENV["DATA_DIR"]

"""
    load_data_file(fstring)

Open file, read as String, split in lines and return Array of parsed floats.
"""
function load_data_file(fstring)
    open(fstring) do file
        return map(x->(v = tryparse(Float64,x); isnull(v) ? 0.0 : get(v)),split(read(file, String),"\r\n")[1:(end-1)])
    end
end

function saveTrace(t::AbstractRawTrace)
    open(DIR_D * "raw/" * t.name * ".jl", "w") do s
        write(s, "return RawTrace(\"" * t.name * "\", Float64[" * join(t.raw, ",") * "])")
    end
end

function saveTrace(t::AbstractFilteredTrace)
    open(DIR_D * "filtered/" * t.name * ".jl", "w") do s
        write(s, "return FilteredTrace(\"" * t.name * "\", Float64[" * join(t.raw, ",") * "], "
        * string(t.filter_preconv_width) * ", " * string(t.filter_preconv_poly) * ", Float64[" * join(t.filtered, ",") * "])")
    end
end

function saveTrace(t::AbstractAnnotatedTrace)
    open(DIR_D * "annotated/" * t.name * ".jl", "w") do s
        write(s, "return AnnotatedTrace(\"" * t.name * "\", Float64[" * join(t.raw, ",") * "], "
        * string(t.filter_preconv_width) * ", " * string(t.filter_preconv_poly) * ", Float64[" * join(t.filtered, ",") * "],"
        * string(t.baseline) * ", " * serialize(t.baseline_region) * ", " * serialize(t.peaks) * ")")
    end
end

function saveTrace(t::AbstractDeconvolutedTrace)
    open(DIR_D * "deconvoluted/" * t.name * ".jl", "w") do s
        write(s, "return DeconvolutedTrace(\""
            * t.name * "\", Float64[" * join(t.raw, ",") * "], "
            * string(t.filter_preconv_width) * ", " * string(t.filter_preconv_poly) * ", Float64[" * join(t.filtered, ",") * "],"
            * string(t.baseline) * ", " * serialize(t.baseline_region) * ", " * serialize(t.peaks) * ", "
            * string(t.region_start) * ", " * string(t.region_width) * ", " * string(t.sweep) * ", "
            * string(t.τ) * ", " * string(t.Δt) * ", Float64[" * join(t.deconvoluted, ",") * "], Float64[" * join(t.τ_sweep, ",") * "]"
        * ")")
    end
end

function saveTrace(t::AbstractFinalTrace)
    open(DIR_D * "final/" * t.name * ".jl", "w") do s
        write(s, "return FinalTrace(\""
            * t.name * "\", Float64[" * join(t.raw, ",") * "], "
            * string(t.filter_preconv_width) * ", " * string(t.filter_preconv_poly) * ", Float64[" * join(t.filtered, ",") * "],"
            * string(t.baseline) * ", " * serialize(t.baseline_region) * ", " * serialize(t.peaks) * ", "
            * string(t.region_start) * ", " * string(t.region_width) * ", " * string(t.sweep) * ", "
            * string(t.τ) * ", " * string(t.Δt) * ", Float64[" * join(t.deconvoluted, ",") * "], Float64[" * join(t.τ_sweep, ",") * "],"
            * string(t.filter_deconv_width) * ", " * string(t.filter_deconv_poly) *
            ", Float64[" * join(t.final, ",") * "]"
        * ")")
    end
end

function saveTrace(t::MinimizedTrace)
    open(DIR_D * "minimized/" * t.name * ".jl", "w") do s
        write(s, "return MinimizedTrace(\"" * t.name * "\", " * string(t.τ) * ", " * string(t.Δt) * ", " * string(t.baseline)
            * ", Float64[" * join(t.final, ",") * "], " * serialize(t.points) * ")"
        )
    end
end

function loadRawTrace(name::String)
    if name[(end-2):end] != ".jl" name *= ".jl" end
    trace = include(DIR_D * "raw/" * name)
    push!(Thesis.DataRaw, trace)
    trace
end

function loadFilteredTrace(name::String)
    if name[(end-2):end] != ".jl" name *= ".jl" end
    trace = include(DIR_D * "filtered/" * name)
    push!(Thesis.DataFiltered, trace)
    trace
end

function loadAnnotatedTrace(name::String)
    if name[(end-2):end] != ".jl" name *= ".jl" end
    trace = include(DIR_D * "annotated/" * name)
    push!(Thesis.DataAnnotated, trace)
    trace
end

function loadDeconvolutedTrace(name::String)
    if name[(end-2):end] != ".jl" name *= ".jl" end
    trace = include(DIR_D * "deconvoluted/" * name)
    push!(Thesis.DataDeconvoluted, trace)
    trace
end

function loadFinalTrace(name::String)
    if name[(end-2):end] != ".jl" name *= ".jl" end
    trace = include(DIR_D * "final/" * name)
    push!(Thesis.DataFinal, trace)
    Thesis.DictFinal[trace.name] = trace
    trace
end

function loadMinimizedTrace(name::String)
    if name[(end-2):end] != ".jl" name *= ".jl" end
    trace = include(DIR_D * "minimized/" * name)
    push!(Thesis.DataMinimized, trace)
    Thesis.DictMinimized[trace.name] = trace
    trace
end

function loadAllRaw()
    for f in readdir(DIR_D * "raw/")
        if f[(end-2):end] == ".jl"
            loadRawTrace(f)
        end
    end
end

function loadAllFiltered()
    for f in readdir(DIR_D * "filtered/")
        if f[(end-2):end] == ".jl"
            loadFilteredTrace(f)
        end
    end
end

function loadAllAnnotated()
    for f in readdir(DIR_D * "annotated/")
        if f[(end-2):end] == ".jl"
            loadAnnotatedTrace(f)
        end
    end
end

function loadAllDeconvoluted()
    for f in readdir(DIR_D * "deconvoluted/")
        if f[(end-2):end] == ".jl"
            loadDeconvolutedTrace(f)
        end
    end
end

function loadAllFinal()
    for f in readdir(DIR_D * "final/")
        if f[(end-2):end] == ".jl"
            loadFinalTrace(f)
        end
    end
end

function loadAllMinimized()
    for f in readdir(DIR_D * "minimized/")
        if f[(end-2):end] == ".jl"
            loadMinimizedTrace(f)
        end
    end
end

function loadAll()
    loadAllRaw()
    loadAllFiltered()
    loadAllAnnotated()
    loadAllDeconvoluted()
    loadAllFinal()
end

serialize(pc::PointCollection) = "PointCollection([" * join(pc.X, ",") * "], [" * join(pc.Y, ",") * "])"
