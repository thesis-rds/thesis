"""Returns the value ln[Γ(xx)] for xx > 0."""
function gammln(xx::Float64)::Float64
	cof = [76.18009172947146,-86.50532032941677,24.01409824083091,-1.231739572450155,0.1208650973866179e-2,-0.5395239384953e-5]
	y = x = xx
	tmp = x + 5.5
	tmp -= (x + 0.5) * log(tmp)
	ser = 1.000000000190015
	for j = 1:6
		ser += cof[j] / (y += one(y))
	end
	return -tmp + log(2.5066282746310005 * ser / x)
end

"""Returns the incomplete gamma function Q(a, x) ≡ 1 − P(a, x)."""
function gammq(a::Float64, x::Float64)::Float64
	if x < 0.0 || a <= 0.0 error("Invalid arguments in routine gammq") end
	if x < (a + 1.0)
		gamser, gln = gser(a, x)
		1.0 - gamser
	else
		gammcf, gln = gcf(a, x)
		gammcf
	end
end

"""Returns the incomplete gamma function P(a, x) evaluated by its series representation as gamser.
Also returns ln Γ(a) as gln."""
function gser(a::Float64, x::Float64, iterations = 100, ε = 3.0e-7)::Tuple{Float64,Float64}
	ap = a
	del = sum = 1.0 / a
	gln = gammln(a)
	if x <= 0.0
		# if (x < 0.0) error("x less than 0 in routine gser") end
		gamser = 0.0
		gamser, gln
	else
		for n = 1:iterations
			ap += one(ap)
			del *= x / ap
			sum += del
			if (abs(del) < abs(sum) * ε)
				gamser = sum * exp(-x + a * log(x) - gln)
				return gamser, gln
			end
		end
		error("a too large, iterations too small in routine gser")
	end
end

"""Returns the incomplete gamma function Q(a, x) evaluated by its continued fraction representation as gammcf. Also returns lnΓ(a) as gln."""
function gcf(a::Float64, x::Float64, iterations = 100, ε = 3.0e-7, floatmin = 1.0e-30)::Tuple{Float64,Float64}
	gln = gammln(a)
	b = x + 1.0 - a
	c = 1.0 / floatmin
	d = 1.0 / b
	h = d
	for i = 1:iterations
		an = -i * (i - a)
		b += 2.0
		d = an * d + b
		if abs(d) < floatmin
			d = floatmin
		end
		c = b + an / c
		if abs(c) < floatmin
			c = floatmin
		end
		d = 1.0 / d
		del = d * c
		h *= del
		if abs(del - 1.0) < ε break end
		if i == iterations error("a too large, iterations too small in gcf") end
	end
	gammcf = exp(-x + a*log(x) - gln) * h
	gammcf, gln
end
